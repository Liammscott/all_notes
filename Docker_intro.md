## Containers 
An *image* packs up everythn to run an app up into an image.  
A container is a running instance of an image.

**Benefits**  
* Portable, once image has been built it can be given to someone else or deployed to the cloud.  
* Flexible, any application can be containerised.  
* Scalable, we can run multiple containers of the same image and share the workload between them.  

## Docker
Docker is the most popular tool but there are others.  
Docker allows you to create, run, and push images.  
Docker is useful for development and release.  

## Dockerfile
Describes environment and how to build your Docker image
```
FROM python:2.7-slim #runtime+tools

WORKDIR /app #Source code, copy everythin from host machine to APP container
COPY  /app .

RUN pip install -r requirements.txt

EXPOSE 80 #Documentation+Configuration
ENV NAME World 
CMD ["python", "app.py"] #start command


demo docker build -t demo:1.0.0 .
demo docker run -p 9000:8080 demo:1.0.0
```
*-p opens port mapping, we want to map local port 8080 to port 9000*

Docker also removes dependency and software after it's used.  
Can reduce image size.  

## Containers in production
Kubernetes to manage containers in production, it can handle scheduling, health-checks, and scaling.  
Kubernetes will move containers around to nodes with enough memory, will prepare more containers of the same image to give more space.




