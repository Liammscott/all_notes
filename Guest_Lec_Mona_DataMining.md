# Text Mining: Maintaining "High" Quality data in Q&A Reports
Mona: Comp-sci PhD, works in gnTeam. > Text extraction, analytics and mining
"How a big bunch of text can be meaningful?"»Applications in life-sciences/medicine (why?)
NaCtem: mainly in medicine and life-sciences, some industry

## E.G.
"The *bat* flew through the air"» what bat?  

Billy hit the ball over tha house"» what ball  

SEMANTIC INFORMATION.  

"Sarah joined the group with some already with search experience"» who has experience???
SYNTAX INFORMATION  

If a person is "sick", are they talking about healthcare or video games?  

CONTEXT INFORMATION

## Machine learning
e.g. pro noun followed by street>> street name
e.g. peoples names 2 or 3 word formula of pro-noun
Elsevier: what is text mining! If we search something we can't read all the possible results, text mining reads all results and outputs useful results » detailed relevant info!
A set of statistical techniques for identifying parts of speech, entities, sentiment and other aspects of text.
supervised vs unsupervised
clustering: data is unlabelled, group points that are close, identify structures, UNSUPERVISED
classification: Labeled data points, what a rules that assigns labels to new points, SUPERVISED
## Data mining
Text mining is the little brother of data mining, which is usually used on structured data. Text mining doesn't usually have as much structure.

## Need for text mining
40% increase in web pages each year, this is a large amount of information and so search engines need text mining to find relevant info amongst this large volume of data
"Extracting meaning from unstructured text"

Feature construction: wanting to understand higher level concepts: what is a cat? a cat is an animal. what are the differences between animals?
## Techniques
Removing stop words, stemming, lemmatisation. are stemming and lemmatisation the same?

mona's project: Carbon disclosure project. Help corporations make carbon emission reduction strategys. » An online reporting system is created.
Nowadays, a fifth of global GHG emissions are reported through CDP.  
Alot of reports are made, but we want meaningful info. » Some general open questions "quantity of emissions?" "have you begun to measure GHG emission quantities?" etc.  
Text mining is used to separate questions from answer etc.

**Problem:** For a double question: who is the highest level of responsibility/HOW are they managing it. Only answering one bit/not answering well. (Question responsibility: people typically only answer 1 part of 2.)
* Other problems: missing answers, multilingual, duplicated answers, long answers, innacurate. Questions are nested, dependable, ambiguous.

» The aim of the group is to manage and monitor the quality of the Q&A system, this includes drafting and analysing questions » finding candidate MFQ, more language dependence than domain dependent knowledge.

## Question foucses features and categorisation method
Ali went to the part
term extraction and dictionaries formulation: take out entities (from libraries)  
spss: from IBM, big in text analytics but has its own language, is long to organise and make your own plugins for.
We create a dictionary, and now look at question focussed features nesting probing multiple etc. We write some rules for category descriptors, "if" means conditional>> do this.  
Built classification model, develop algorithm: correlate MFQ and top 5 features of question profiling and representation. Which features are most correlated with MFQ?

Socrata Open Data API
PRACTICAL TEXT MINING AND STASTICAL METHODS
