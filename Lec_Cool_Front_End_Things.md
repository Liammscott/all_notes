# Fonts
Google Font: List of good fonts (Used this for the dashboards)
Font Awesome: Large collection of icons. (User responds better to icons than text)

# The box model
Width: width + border+padding+ margin
use
```
padding:0
border:0
box-sizing: borderbox
```
width = width + margin

# Flexbox
Great way to solve multiple CSS/layout challenges
Elements grow and can be placed horizontally  
parents have `display: flex` property
then children have `flex:<value>`. So if they all have `flex:1` they all take up the same space, `flex:2` takes up twice as much.  

# Sass
CSS pre processor that takes in '.scss' and '.sass' files making normal '.css'
Allows:
    * Nesting
    * Variables
    * Mixins (like functions)
    * Partials  

## SASS ampersand
Used to add alternative variations of an element within it's property.  

## SASS Partial
It's good to split code into different files. This ihard to do in CSS.
With SASS name our file with a proceeding underscore and the `@import`  syntax.  

## A SASS variable  
can set colour to a specific variable name and just call that rather than hexadecimal.  

## Responsive Design
Media queries can be used to control layout dependent on the screen viewing the page.
Can use these in nested blocks.   

# Component based frameworks
*Big in UI now. Big on react and angular*
## Why use components
Applications can be naturally split into components, each component may contain:
    * some logic - JS
    * Some state - JS, Memory  

## Component based framework to split the separation of concerns

# Getting started
`Create-react-app`
`vue-cli`

# Redux
UIs have to store alot of state: Product list, basket items, customer info, probably more things.  
State management tools such as Redux help us to manage state.   

# Webpack
Bundles javascript files/dependencies into a single src.
Makes smaller code to be redeployed, faster to load.  
Browsers have to negotiate TCP connections for each resource so need to cut down.
    (This is a similar job to a compiler)   

# Babel
Can transpile ES6 to ES5 and other thigs.  
When writing Apps you want to use the latest syntax to stay relevant to longer.  
Babel allows us to use latest language features without losing broswer compatibility.  

# Jest  
## Testing code  
Jest is used to test JS code.  
Multiple unit testing tools, but Jest is most popular  
Combine Jest with Enzyme for easy reactJS testing.  


> Good editor: Brackets is worth looking at.  
