# How the Web Works  (WWW and HTTP)
James is OG software dev and now works on voyager in the warehouses. 
We write **Client-Server** applications. The client is usually a browser on a phone or laptop, with processing happening on browser.  
Internet is a multimodal network, v failiure proof as there are so many connections and routes.  
We only write whats on the server, internet and browsers controlled by companies.  

**URLs**  
uniform resource location
`<protocol>://<host><:optional port>/<path/to/resource><?query>:`  
port number:  If a host does multiple functions, email server and something else, port tells you which one you're accessing.  

**HTTP**
  * Connectionless: you ask the server something, it answers, it disconnects.  
  * Stateless: second request DOESN'T know first one has happened.
  * Media Type Agnostic: As long as the browser can undeerstand you can send anything!
*90% of the internet uses HHTP protocol*

**Request response**  
Clien>>give me page>>Server
Server>>Build Page>>Here's the page>>Client

Very scalable!!

**Messages**  
HTTP Request:  
  * Verb, do this, GET, POST
  * URI, thing i want to access
  * Version, which HTTP
  * REQUEST HEADER, Name value pairs.
  * Request message

HTTP Response:  
  * Response code
  * HTTP version
  * Response header
  * Response Message  


**Cacheable** Cache are between client and server, with GET: We know the request is good and exists, we can store it "somewhere" on the internet.  
*to deal with updates between page requests, the Response has a code saying how long the data is good for.*  
*Peering between companies is controversial atm*  
*When you're in the internet you're obliged to send data that isn't yours*  
*Ideally we want ot cache things near the client, we can enforce this with CDN (Content distribution network)*  

**Status codes:**
  * 2xx OK/No error/no content
  * 3xx Redirect to new URL, moved permanently, Moved temporarily
  * 4xx Client error, bad request/forbidden/not found/I'm a teapot
  * 5xx Internal server error, ( *we get these in our code* ).  
*Try it on your blog: run blog, connect with telnet*  

**Cookies**  
Help for logins, remember who a person is. *Name from UNIX term for small amount of data*  
Server stores a cookie for you as a client.  
To make cookies secure if we're storing data that is important (login) we send it over http, encrypted.  

*3rd party cookies*

# How the web works 2: DNS, SSL and HTTPS  
*Last lecture we talked about requests to serves, returns, and cookies (and their security issues)*  
When we put in a URL: *www.THG.com* we need to **resolve** the address, find the IP. This is a **DNS** lookup.  

This look up needs to be REALLY scalable, because there are loads of internet addresses.  
If our ISP can't find the URL/IP then it looks it up the root *'.'*, *there are about 900 of these*. ISP is configured by default by who ever you buy your internet off. Configured differently in a big business like ours. This is a service provided.  Can give wrong address (like when bt blocks pirate bay or something).  
Root often doesn't know, so it tels us to look in the *.com* server, which tells us where the **NameServer** that does know is.  
Ask *.thg.com* where the address is.  
*This lookup is rapid*  
*Once we find the address we keep it in the cache, next time it is looked up we can find it*.
> Whats up with this cache, does it fill? is it not hard to lookup when it is full- Data is small, name mapped to 32bit data, good storage of cache, easy to look up.  
> Denial of service attack on the root at one point was nuts. 
> Use IP to name addresses, IPB4 had 2billion, IPB6 has a million times more.  

## TLS and HTTPS (Transport layer security, formerly SSL)  
### Encryption terms:
  * **Key** thing that converts plain to encrypted and vice versa
  * **Plain text**
  * **Encrypted text**
**Symmetric encryption:**  
Data encrypted with key, decrypted with same key.  
If you could send a key safely, you'd also just send your data this way.  
**Asymmetric encryption:**  
Public key encrypts on client.  
Private key decrypts on server.  

*Can sign things, encrypt with private key, validate with Public Key on Client to check where it has come from.*  

>Investigate these things, there will be some links. 

## TLS
Difference between sym and asymm is performance, server and client negotiate a shared key and then use this for symmetric encryption.  
*How do we choose a shared key? How does the client get the public key? how does the client know the public key is for the right server?*  

**Shared key:**
  * Random number + Nonce, each side chooses random number and concatenate with each other.  
  * Diffie Hellman key exchange (theres a link for this)  
  * the shared key is agreed upon by the private and public key,  and then this is sent to client and server (in an encrypted way?).  
**Servers public key:**  
  * Certificate Authority
    * Issues a certificate signed by CA containing servers public key, says this is teh correct key for server "hostname". We then trust this.  
    * Trusted CA list is built into browser.  
    * Certificate authority has a very private key that it uses to make the certificate.  
  During negotiation this certificate is sent and you can check it, confirm that this is the correct Public key. This is the "trust" step.

>Life cycle: set up ssl session,  server sends back certificate, they agree a one time encryption key for this session, then, the communication begins.

Putting it together: segregate secure,insecure traffic to diff ports and scheme HTTPS.  
If DNS server is compromised, go to wrong server, shouldn't be able to decrypt because there isn't the agreed upon shared key.  
**cookies** transferred on secure and unsecured connection, can give cookies secure flag so they are only sent on secure connection.  
*Sometimes this goes wrong, site doesn't match certificate, certificate expired, there is no certificate "would you like to continue?"*  
*Data is still encrypted but is encrypted with the private key of the person behind the weird IP address.*


TheHut.com requests info from CA, CA interrogates sends certificate signed by CA private key. Then client requests from TheHut, sends certificate with the public key from the website. Now you can begin negotiating a shared key.  
*You should look into this bc it sounds confusing*    
*Amazon offers a certificate authority service*  

> What determines your private key?  


# How the Web Works 3  
## Cacheing  
Caching is done as a proportion of your disk drive.  
**Expires heading:** Data has an expiry date, this is cached until it goes out of expiration and so things that are used a lot *e.g. Twitter logo will be stored*.  
**Last modified:** request, web server finds files, checks date, Server replies, browser Loads this from page.  
**Etag fingerprints:** If-None match, request file file if it matches tag, if not changed then load from browser.  
*Have to be careful of changing our cacheable objects so that when we do make changes peoples caches update*  
*THG use expires header for images*  

## Static CDNs
Service that pushes your static content to the edge of the internet.  
At session start, you secure key SSL with Edge Server, this then sets up with Original Server (This second server-server connection is set up already).  

## Dynamic/Full Site Acceleration  
* Decreasing page-loading times  
* Offloading web-servers  
THERE ARE ALOT OF TECHNIQUES (including compression).  
* TTL bending, site acceleration tool monitors web server, as your servers go under load they slow down, as this happens the acceleration tool starts caching more data for longer.
* Off loading SSL termination, SSL termination algorithm is heavy so security is reduced when server load is high.  

* Route optimisation: Dynamically optimise routing (Fastest vs Cheapest). Full site acceleration service has a couple of routes to send your data, will track the times and choose the fastest.  

> Eventual consistency: If you watch a massively retweeted tweet, the number of retweets oscillates. THIS IS DUE TO CACHING THINGS.  


*These processes are slight, shaving off milliseconds, but when 100s of objects are on the page this approaches visible time.*  


