# Modern JS
Scripts are provided/executed as plain text (just readable text, not objects/graphical representations). Can be run on any devicee running a JavaScript engine.  
## Engines
1. Reads/parses the script
2. Converts to machine language/compiles it.
3. Runs the code.

Optimisations are applied at each stage of the process.

## In browser JS
*JS is "safe" language, it doesn't provide low level access to memory or CPU, as it was created for browsers that don't require it.  
*It can send requests over the network to remote servers, and download/upload files (these are AJAX/COMET technologies.)  

**Limitations/what it can't do**
* JS can't read or write to arbitrary files on the hard disk, copy them, or execute programs. It has no access to OS functions.  
* Windows/tabs don't generally know about eachother. Some times if one window uses JS to open another one this rule is broken, but even then the pages may NOT access eachother if they are of different URL.  

### AJAX 
AJAX is Asynchronous JavaScript, a set of web dev techniqus on the client side to create asynchronous Web Applications. With Ajax web applications can send and retrieve data from servers asynchronously (in the back ground) without interfering with the display and behaviour of the existing page.  
Decoupling the data interchange layer with the presentation layer AJAX allows web pages and web apps to change content dynamically without needing to reload thewhole page.  

## Selling points

> * Full HTML/CSS integration
* Supported by all major browsers and enabled by default




# Pure and Impure functions
* **pure** functions given the same arguments, always return the same values.
* **impure** functions do NOT behave like this.
Example:
```
const xs = [1,2,3,4,5]
//pure
xs.slice(0,3); //[1,2,3]
xs.slice(0,3); //[1,2,3]

//impure
xs.splice(0,3); //[1,2,3]
xs.splice(0,3); // [4,5]
```
Other examples include the use of ```stdin``` and things with side effects.   
>Key: We should separate our pure and impure functionality in the code.

**Impure** and **pure** functions help us to keep in mind the
principle of **single responsibility**.

# Global objects and windows
**Global variables**  
```
// Global a
var a = 10;

function foo() {
// Non-global b, only in function
    var b =20;
}
```
**Global object**  
Global object is typically the page that is loaded that calls the JS.  
We can actually call the window object with `window`, and we can then look at a list of global variables.
Global variables are all properties of Global object.
```
var abc = 10;
//same as
window.abc
```

## let and var
`let` is block scoped. Can be smaller than function scope, consider `for` block.  Can also not be redefined in same scope.
`var` is function scoped. Can be redefined in same scope also.  



So when we just define functions in the JS we are polluting the global scope!

# Conditionals
```
if (1+1 === 2){
    // block
    const x =1;
}
else if (2-1 < 3){
    const y=2;
}
else{

}
const x = 'hello world';
switch (x){
    case 'hello world':
    // lines
    break;
    default:
    //lines
    break
}
```
* `switch` is evaluated once, value of expression is compared to each case once.
* Value of expression is compared with values of each case.  
* If there is a match, the associated block of code is executed.  

# Callbacks
**Simply put** a callback function is executed after another function has been executed.  
**Complexly put** Functions are objects in JS, because of this functions can be higher order and have other functions as arguments.  

## Why we need callbacks  
JS is event driven, this means that instead of waiting for a response before moving on, JS will keep executing while looking for other events.  
Consider:
```
function first(){
  // Simulate a code delay
  setTimeout( function(){
    console.log(1);
  }, 500 );
}
function second(){
  console.log(2);
}
first();
second();

>console: 2
        : 1
```
Callbacks are a way to make sure a certain code isn't executed until the other is executed. Can be written as follows:
```
function doHomework(subject, callback) {
  alert(`Starting my ${subject} homework.`);
  callback();
}
function alertFinished(){
  alert('Finished my homework');
}
doHomework('math', alertFinished);
```
With the alert finished being the callback function.  

Real world application:
```
T.get('search/tweets', params, function(err, data, response) {
  if(!err){
    // This is where the magic will happen
  } else {
    console.log(err);
  }
})
```

This code makes a search request to twitter, based on response of function does a thing. We need to check what the server response to out request is before proceeding.  

# Declaring functions
## Function expression
```
const f = function(x, y) {
    return x+y
}
```
## Function declaration
```
function g(z,k) {
    return z-k

}
```
Note: when JS compiles it looks for all declarations and puts them at the top of the page *Hoisting*.

```
g(1,2)
function g(z,k){
    ...
}
```
is valid.
but
```
f(5,7)
const f = function (x,y){
    ...
}
```
is not!

more complex:
```
const h = function(){

    return f(7,5)
}
const f = function(x,y){
    ...
}
f(5,7);
h()
```
is also valid!



## Arrow function
We can define functions using the arrow function!
```
(param1, param2) => { statements }
(param1, param2) => expression
(param1, param2) => { return expression; }

(singleParam) => { statements }
singleParam => { statements }

() => { statements }
() => expression
() => { return expression; }
(param1, param2, paramN) => expression
```

```
//Normal
var multiply = function(x ,y){
    return x*y
}
// Arrow function
var multiply = (x, y) => { return x*y }
    //or
var multiply = (x ,y) => x*y;
```

Arrow functions do NOT define their own `this` context, they work with the `this` in the scope they are in.

### Syntax
Arrows cannot have line breaks.  
To return objects need parentheses around `{}`.  


# First class functions
*First class functions are functions which behave like any other object. That is, we can pass a function can be passed as an argument to another function, returned by another function, and assigned as a value to a variable.*

## Higher order functions
A higher order function is then one that takes a function as an argument or returns a function. Noice.
