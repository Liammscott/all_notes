# How and what

* We test our code as we go.
* We test functions *we don't even have yet*.
* We don't proceed with writing the functions until we fail these tests (as expected).
* We write **the minimum** amount of code required to pass these tests.  

# Why

* Writing method calls into the command line is really long and repetitive, we want to automate this. (Your time is important!)
* It keeps us **focussed**. We want to solve **one test** at a time, not make the whole app!  
* It makes the code readable to other people and gives a good legacy.  
* We can check if features are compatible (and find last working point). 
* Prevents excess code being written.
* Most importantly: **So we know our code works**.  



