# Lec 1
## Definitions
* **Declarative knowledge:** "What is the square root of a number?"
* **Imperative knowledge:** "How do I find the square root of a number?"
* **Turing machine:** Programming language/commands can be stored in memory
* **Abstraction:** Remove the "under the hood" complexity of a program or hardware. We **abstract** the operations of a computer reading/writing to memory with commands like `read` and `write`.
  * Large systems are understood in levels
  * Interfaces allow us to change implementation without effecting higher levels
  * Syntax defines what a valid construction is
  * Semantics tells us what syntactically correct string does
> We abstract **imperative** knowledge into sub functions and simplify the code. **You're done when it reads like english.**


* **REPL:** Re-Evaluated-Print-Loop, allow for line by line printing and entering of code
* **Shebang:** Unix shell bang line, `#!/bin/sh`
* **Turing complete:**Alan Turing created a machine that can take a program and run that program and show some result. But then he had to create different machines for different programs. So he created "Universal Turing Machine" that can take ANY program and run it.
* **Lambda calculus:** the smallest universal programming language: any computable function can be evaluated in the context of λ-calculus and evaluating programs in the language consists of a single transformation rule: variable substitution.



# Lec 2

## Definitions and concepts:
* **Iteration:** Method for looping over pieces of code.
* **Functions and scoping:**
def name_of_function(list of formal parameters):
body of function
Formal parameters and local parameters exist only with in the scope of the function.
When the function is called it checks for names it needs in its local environment. If it csn't find it looks in the next highest level of the code.
* **Lexical/static scoping:** A convention that sets the scope of a variable so that it may only be called from within the block of code it is defined in.
* **Recursive functions:** define a base case, and a function that takes an input and then returns easier to resolve arguments to the function. The function calls itself recursively until it reaches the base case.


# Lec 3
## Structured (non-scalar) types and mutability:
**Tuple:** 
  * Immutable ordered sequence of objects.
  * Tuples don't store objects, rather references to where they are in the computers memory.

**Range:** A function that returns an object of type range that can be iterated over.
**Lists:** Do everything tuples do, but they can be altered in the run time of the program i.e. *They are mutable*
  e.g.
    `.append()` `.remove()` `.sort()`
# Lec 4 (tock)
**What are strings?**
Strings are arrays of bytes, each representing a unicode character. 
There is no `char` class (as in C++), only strings of length 1.
Python DOESN'T use UTF-8 internally, as each character can take between 1-4 bytes, so whilst being storage efficient it is difficult for python to iterate through an array where each entry is a non-uniform size.
So instead, python will represent the code point of the character as a 4byte binary digit.

## **What are Tuples?**
Tuples are fixed size,each entry in a tuple may take up a varying size of data (e.g. strings of different lengths)
Tuple makes an array of size=number of entries, with each space being a pointer to where to find the contents.

*collision resolution*
hashes change when dictionary is grown

## **Associative array/dictionary**
Key/value pairs
can add a pair to collection
remove a pair from collection
modify existing pair
lookup a value associated with the key
Each roe (hash, key, value) takes up 24bit in the architecture. 
For a hash created which ends in the same digits as a preexisting has, this is a *collision* and the hash is inserted into another row.
Dictionaries are created with 8 spaces and at 2/3 full the dictionary size is increased, at this point the hash values for the previous entries must be changed.
The dictionary is resized because at 5 rows filled the probability of a collision is too large, the dictionary doubles in size.
**Hashing:** a value is computed/munged from data, the value of the data is in someway represent a value is computed/munged from data, the value is in some way representative of the data.
We then use a hash value of tuple/data structure to find an entry in the dictionary.
Lookup in a dictionary using hashes is a *constant time process*

# Lec 5 tick
*catch up, last time we looked at list comprehension and dictionaries*
Note, generator is less computationally expensive only output terms we need. A list will store the info (which is good if we want to refer back to it)  
Dicts support insertion, deletion, and lookup in constant time (which is good).  

## Exceptions
Errors can be:
  * **Syntax:** grammatically incorrect code, caught at compile.
  * **Runtime:** encounters something it can't handle at time code is run (e.g. doing weird maths with strings)

Runtime errors can be managed so that they are not fatal (terminate the program)
```
try:
    do your operations here
except Exception2:
    do this block if Exception2 happens
   ```
where `Exception2` is a type of error (like ZeroDivisionError ValueError etc.)
*Note: you do have to know the name of your error to throw and catch exceptions for it*  
We can also use exception handling to control program flow.  
*e.g. if we try use a key in a dictionary and that key isn't there, the program will crash. So we can make an exception to handle this case (such as adding that key to the dictionary)*
We can except *KeyboardInterrupt* from ctrl+c, in a `while True:` loop giving it a value `exit(0)` to exit the program 

## Throwing exceptions with raise
Can pass failiure data conveniently back to "shallower caller" across nested function calls"
```
except Exception as e:
  do something
  raise(e)
```
```
except exception as e:
  (except, intance, tb)=sys.exc_info(e)
   ```
## Assertion
assert Boolean, (Throws assertion error)
EXCERCISE>> Can you write an assert as a function?

# Lec 6 

## Objects
What are objects?  
Can call operators, functions, methods.   
> e.g. for a string: can call len(str) can call '+', can call "str".upper()
*Functions abstract operations, we define our own functions to add to the vocabulary of python*

**Abstract Data Types:** an ADT is a set of objects and the operations on those objects. These operations provide an interface.
*e.g. for our rational number type, we built our own interface and defined addition, subtraction etc.*
*Note: the type of the result of an operation with our rational number is a **tuple**.*

## Classes
Classes are Pythons mechanism fro making new types and methods associated with that type.
* **Methods:** functions that are tied to the class, operating on a particular objects data.
* **Instantiation/construction:** Create new objects of that class type.
* **Attribute reference:** using . notation to reference methods

excerpt from class Rational:

```
   def __init__(self,numerator,denominator):
    if denominator<0:
    numerator,denominator=numerator*-1, denominator*-1
   g=gcd(numerator, denominator)
   self.numerator=numerator//g
   self.denominator=denominator//g
   def string(self):
    if self.denominator==1:
      return str(self.numerator)
    else:
      return "{0}/{1}".format(self.numerator,self.denominator)

   ```
self is a pointer to the objects own instance  
`__init__` is reserved, calling a=Rational(1,2), Python looks for a class called Rational, then looks for `__init__` in the class and passes control over to it.  

> e.g. could change def `string(self)` to `__str__(self)`, we could now type `str(a)` and present Rational numbers as a string using the in built Python `str` protocol. Python can now look in the objects class for a `__str__` method. A consequence of this is we can call `print(a)` which tries to convert and object to a string and print it to the REPL.
> Could even call `print(a + b)` where `a` and `b` are class Rational.

## Special method names
All these \_\_command\_\_ are examples of **special method names**

> Excercise: Polymorphism, make the rational class accept decimal numbers as arguments and also print integers


# Lec 6
*Nota bene: use underscore to emphasise that data is "private"*
*Use getter and setters to define and receive data for a type*  

## Class vs instance variable

```
class Dog:
  kind='canine' #class variable shared by all instances

  def __init__(self,name):
    self.name=name #instance variable unique to each instance
  
  def __str__(self):
    return "{0} {2}".format(kind, self.name)
------------
a=Dog('lula')
b=Dog('pico')
print(b)
> canine lula
print(a)
> canine pico    
```
## Inheritance
"Things inherit features from the base classes from which they are derived"  
```
class Foo():
  def __init__(self):
    print('do nothing')
------------
a=Foo()
> doing nothing
dir(a)
>yields loads of funcions
```
this is because it is a subclass of base class `object` by default, it is basically `class Foo(object)`.  
most of python can be written in python, pypython (This sounds bizzarre).  

consider base class `Person` derived from base class `object`, initialised with:  
```
def __init__(self,name):
  self._name=name
  self._birthday=None
   ```
With a `get_birthday`, `set_birthday` etc. as well as a `get_age`.  
Can then make `class Employee(Person)` with  
```
class Employee(Person):   
  next_id_num=0
  def __init__(self,name):
   Person.__init__(self,name)
   self._id_num=Employee.next_id_num
   Employee.next_id_num +=1
  def get_id(self):
    return self._id_num
  def __str__(self):
    return "{0}: {1}".format(self._id,self._name)
   ```
*"Most derived class wins", so employee is type person, both with function `__str__`. But calling `__str__` will call the Employee function not Person's*  
We want to hide access with get and set functions, *e.g. forbid `e._name(Sally)`. for instance, setting name not surname might be bad*  

Consider: From the last function, format is an instance of string, we pass it a tuple.  

```
def tester(c,*args, **kwargs)
    print(repr(args))
    print(repr(kwargs))
    print(c)
```
kwargs is key word arguments

`*args`: star is important, find out
`**kwargs`: double star is also important, find out


# Lec 7 TDD: Test Driven Development
unittest is a good way to automate testing, runs VERY fast.  
Testing helps you write your code so it is easier to test! *This usually makes your code more modular*  
Test code should be relatively short to minimise bugs in your test case.  


```
   import unittest
   import string_function as st
    
  def testfunc(unittest.TestCase):
    def setUp(self):
      print('setting up')
    def tearDown(setUp):
      print()
    def test1(self):
      print("running add test") #this is a nice thing to do so you can see which tests have failed
      result=st.string_add("one","two")
      self.assertEqual(result,"onetwo")
    def test_compare(self):
      result=st.compare_strings("one","two")
      self.assertEqual(result,"onetwo")
 if __name__='__main__':
  unittest.main() #invokes runner on the extended class
 ```

*You should lead with the test cases, change the CODE later*
Consider: Test first approach to problem set 2.  

## Lec 7 Decorated variables
Python DOES support private variables, 
```
self.__privatevariable=variable   
   ```
canNOT be accessed outside of the class.  
The instance variables of an object are stored as variables inside the object.  
*Notes: 
  * Functions are first class objects: so we can do anything to them that we would do to a normal object.  
  * We can define functions in other functions.*
Lambda function:

``` 
  lambda "formal parameter": thing you will do to formal parameter  
```


* **Closure:** function1 has an argument, function2 is defined in function one taking a second argument and can also access the argument from function1.  

## Decorator syntax
```
@count_invocations
def new_greet(name):
  return "hi"+name
    
```
Properties decorators with getters and setters:  
```
@property 
def name(self):
  return self.__name
   ```
so can call
```
print(p.name)   
   ```
which looks like we are directly accessing a variable but we are actually calling a function.  
*Note: Youd should look at the notes for the setter function used as well BC it looked the same*

# Lec 8
An amusement: generating random speeches *can we generate a speech in the style of another author?*  
> There are real world applications to this: predictive text etc.

*Ideas and code from Omer Nevo's talk on youtube*  
**Approach1:** 
  * Given words in the authors work, choose randomly from them. Choose high frequency words with a higher probability. 
  * Can copy words into a list, duplicates help because they encourage a probability distribution of word selection.
  * Then use `random.choice` to select a random entry in the list. *It is a variant of random.randint(0,len(author_text))*

```
for _ in range(30)   
   ```
is python convention for 'do this 30 times, i don't care which time i'm on'  
*Approach1 does generate text, structure is lacking. We should choose next word based on previous word. We will use Markov process, probability of moving to a next state is just a function of the current state. NOT it's journey to its current state.*  
We can implement this with a dictionary, `{word1: [word2,word3,word4,....wordn]}` different values of probability for each word. Can improve to *higher order markov model* involving preceding words with a bigger dictionary.  

```
class Markov:
  def __init__(self,order): #order is flexible, lets us do multiple markov model orders
                            #higher order we use multiple values as a key.
    self.order = order
    self.group_size=self.order + 1
    self.author_text = None
    self.next_word__for = {}
    pass
  def train(self,text)
    delf.author_text = text
    #iterate over self text
    for i in range(0,len(author_text)-self.group_size):
      key=tuple(self.author_text[i: i +self.order])
      value=self.author_text[i+self.order]
      
      if key in self.next_word_for:
        self.next_word_for[key].append(value)
        else:
        self.next_word_for[key] = [value]
    pass #pass means don't do anything
  def generate(self, length):
    #choose starting word randomly iteratively look up next given current
    index = random.randint(0,len(author.text)-self.order:])
    result=self.author_text[index:index+self.order]

  for i in range(length):
    state = tuple(results[len(result)-self.order:])
    next_word = random.choice(self.next_word_for[state])
    result.append(next_word)

  return " ".join(result[self.order:])
    pass
   ```
## Lec 8
> Peter Norvig's PyTudes worth reading!  

We're thinking of making the game of Life using Norvig's approach, we will need one new concept: `set`.  
we needs, World, Cell, live/empty, neighbours, next generation, display, live neighbour count.  
We will use `tuple` for live cell, using x,y coords.  
**Counter class:** A dict where the value of each key is the count of number of occurances of that key.  
we use this for for a cell how many live neighbours are there
```
   class Counter(self,iterable):
    for i in iterable:
      if in self:
        self[i]+=1
      else:
        self[i]=1
   ```
**Sets:** {1,2,3,4,5} & {2,4,6,8}={2,4}

```
   def neighbours(cell):
   (x,y)=cell
   return{x,1,y-1 x,y-1 x+1, y-1
          x-1,y,            x+1,y
          etc.}
   ```
For each neighbour in neighbour cell

## Lec 9
> How efficient are our algortithms? orders of growth? BIG O NOTATION?



## Understanding efficiency
Typically, this takes into account time, and space. We want to be able to reason how long/how much space it will take to solve a problem.  
We care:
  * because users care (delay drops conversion! *amazon* ) 
  * because we have alot of data to search through *google indexes 1.3e14 pages*.  

**Space vs. Time efficiency:** Dicts are *high* memory, *fast* lookup. Lists are *Low* memory, *slow* lookup.  
*We focus on time!*
Considerations for an algorithm: Have I *Implemented* my algorithm well? Is my algorithm chosen well *Iterative/recursive*?  
> We can MEASURE all these things like in Physics, recording times for processes. Note that measuring time is hard, granularity (smallest time can measure), computer has other stuff to do with CPU. Internal effects like memory caching are important and effect measurements too.  
> Can count number of operationgs
> Or can use order of growth.


)* Measuring time is hard, can I reason what will happen for big numbers based on small numbers? 
* Counting number of operations is hard, consider the recursive `fact(x)` function, recurses `x` times. ( *linear in x* ). There are a couple of *DIFFERENT* types of operation going on, can I compare them *what is a primitive/basic operation?*

## Orders of growth
> What happens when inputs get BIG?

Different algorithms change how the program runs, consider iterating a list to find an element.  *We will typically assume the worst case scenario*.  
* **Constant:** floating point math, dict
* **List:**  Linear
* **Quadratic:** List in list



## BIG O
*f(x)=O(g(x)) x->inf*
**Simplifications:** drop multiplicative terms, only focus on leading term.
**Analysis:** O() is *additive* for *sequential* statements, O() is *multiplicative* for *nested*.
*Example: log(n) behaviour is seen in bisection, log2.*

## Nested loops
Intersection of lists:  
  Naively:
    ```
       for i in a:
          for j in b:
            if i==j:
              return True
       ```
This is a case of nested statements of *linear order*, this yields *quadratic order*. 
*Note: comprehensions look nice, but they just do this.*  
A more linear problem solve would be to iterate through each list once and append to a dict ( *linear* ) and then look up through these dicts in const- *ish* time.  
```
for i in a:
  mydict[i]=True
for j in b:
  if j in mydict:
    result2.append(j)
   ```
Memory for this is LARGER, this method will only yield benefits at high number.  

# Lec 10, Understanding the efficiency of programs

Question *(Jade)*: `if key in dict`, why is this faster than `if key in list`.  
*This is what we will look at today*  

> Big O ranking: O(1), O(logn), O(n), O(n log n), O(n**c), O(c^n)

## Log complexity: Binary search.  
Based on sorted list, bisection search can find an element in log_2(n) time.  
*This means every double of solution steps increase by 1*  
We find the mid point, is number above or below?
Divide, repeat.  
list size decreases following lof.  
*Issue: Python keeps making new lists, this takes linear time. Can just move iteration along in actual list.*  

## Log complexity: int to string
log_10(n) BC our numbers are in base 10.  

> Note: algorithms reducing problem size by 1 each iteration are LINEAR, problems that divide the problem size are LOGARITHMIC.  

## Polynomial Complexity  
Nested loops.

## Exponential complexity
More than one recursive call for each problem size.  
Many important problems are exponential, leading us to look for approximate solutions we can calculate quickly.  
> Fibonnacci(n) comes out at 2**n  

*the iterative solution is linear*
*Can you do the power set with merge sort*


## Back to dict vs list  
list: 
  * stores pointers to location of value. Calculation is performed, array starts at memory slot 0, 0th element = memory 0+0*slots, 3rd element = memory 0+3*slots.  
  * index, stpre, length, append are all O(1).
  * == is O(n) (we need to check each element of both lists).  
  * remove is O(n), copy of list is made with element gone.  
  * copy is O(n)
  * reverse is O(n)  
  * iteration O(n)  
  * In list O(n)  

dict:  
  * Fundamentally an array of pointers like list, but offset is done by looking at hash and offset
  * Index O(n) with hash  
  * Store O(n) if all n have the same hash. A list of values is then stored at that hash.  
  * Average case O(1) for all operations.
  * Extending Dict and rehashing to new space is O(n)  

Questions (mainly about n)
> should we ever disregard Big O? like for a given n can O(n**C) be worse than O(c^n).
> How do I check if n is big enough? does it depend on the function? what if the data set never gets big enough? 
> Is space/time solution always a trade off?
> Does the space issue eventuall equate to a time issue?
> Why is calculation of hash index faster? 

# Lec 11, Search and Sort  
Consider: 
  * A linear search is O(n) for sorted and unsorted list. 
  * if we want to sort a list for Binary search, either we want to search enough to make to overhead worthwhile, or we need a sort less than O(n).  

## Bubble sort
Compare consecutive pairs, swap elements in pair. finised when list is sorted.  
*This is O(n^2), for loop in a for loop*  

## Selection sort
Iterate through list until you find the smallest element, then place this element in the first position.  

## Merge sort  
Crux: If we have two sorted lists we can merge in linear time. 
We use two `for` loops but NOT nested.  
Divide and conquer: If list has 1 or 0 elements it is sorted. If it has more we split it. This decreases the size of the problem, giving it a log feel.  
We iterate through *linearly* to compare elements, and we recursively split the list into smaller elements *logarithmically*.  
This nesting gives a multiplication of the orders.  

*Consider in a merge sort, one side of the data only sees the other at the very end, so we can split the compting across multiple cores*

**We've just finished MIT semester 1! now there will be ALOT of tock lectures**  
# Trees (tock1)
Trees give a hierarchical nested structures, **merge sort**, **recursive fibonnacci**, **file systems**.  

## Tree terminology  
**Nodes:** points in the tree connected by **edges**.  
**Root:** bottom special **node** with *no parent*. *if there is more than one root it ISN'T a tree!!*  
**Parents:** Every **node** has only one **parent**.
**Symmetric:** if *c* is the child of *p* then *p* is the parent of *c*.  
**leaf:** node with no children.
**interior node:** One or more children.  
*Trees are directional! there are no cycles*  
**Sub tree:** A node and all it's descendants in the tree. *This lends itself to recursive operations. Anything you can do to a tree you can do to it's sub trees*  
**Height:** longest path from **node** *n* to **leaf**.  
**Depth:** depth of **node** n is length to **root**.  
**Left to right order:** distance of left **branch** to **leaf** and right **branch** to **leaf**.  
**Binary tree:** 0 or 1 left child, 0 or 1 right child.

*Consider:* 
  * expressions as trees. Read left to right, some **nodes** are operations and some are numbers. We consider sub trees and evaluate the expressions they represent. 
  * do this defining a tree class.
`Node` that can have left and right children and data.  
`math  operations` that can be Node data.  
**Leaf** nodes are numbers, `else` it is an operation.  



would construct like:  

```
root = Node(data = '+')
root.left = Node(data = '3')
root.right = Node (data = '*')
root.right.left = Node('+')
etc. . . 
```
In this code/object the operation will be recursive.  
The functions will get the **leaves** of the **tree** which *have* to be numbers. Then progressively perform operations on these numbers.  

>EXERCISE: Make this tree with algebraically correct parentheses.  

*This **tree** structure is common and is a linked list structure*  


## Tries
*from re **trie** val , but prounounced try*  
what words are possible from a dictionary.  

h(r= i(r=s, l=m), l=e(l=r) )  
> Excercise: problem sheet 4.

> Excercise: convince yourself that writing the data to a list is linear. Also unsure of the sorting method.  



*Insertion time is ~ logarithmic*  
Commonly, structures are used for search trees.  
Pathological case: Inserting a sorted list, becomes order n.  


# Tock2 (More trees)  
## Sets 
Sets are containers of unique objects.  
*No literal notation for empty set.*  `empty = set() `  

# Tock3 (Iterables, iterators, and generators)
Can use function `iter()` to retun and iterator to be used by a loop.  
`iter()` checks whether the object implements an `__iter__()` method, if it does it uses it to obtain an iterator.  
iF `__iter__()` is not implemented but `__getitem__()` is then python creates an iterator that fetches items in order using `__getitem__()`. This means all sequences are iterable.  
*Iterator* objects themselves expose two methods:
  * `__iter__()`
  * `__next__()`

```
s=hello
it = iter(s)
while True:
  try:
    print(next(it))
  except StopIteration:
    break
```
To go over the sequence again we must build a new iterator.  

## Generator functions and `yield` keyword  

```
def odds():
  i = 1
  while True:
    yield i
    i = i + 2
o = odds()
print(type(o))
```   
`odds` is now a generator class (because we have included yield in it.)  
Generator is good because it only returns a value when we need it.  
> Excercise: Make `range`.  

* Any Python function with a `yield` keyword in its body is a generator function which, when called, returns a generator object. In OO language it is a *generator factory*. 
* Generators are iterators that pass the values over to `yield`  
* if g is a generator, it's also an *iterator*, so `next(g)` fetches the next item produced by `yield`.  
* *Generators* are *lazy*, they produce one item at a time.  


**Generator comprehensions**  
```
g = (x**2 for x in primes_range(10,50))
print(type(g))
```  
`g` would be a *generator*.  

> Generators are good for our tree class.  


# Tock 4 
## Generators cont.
Some generators for transforming sequences into new sequences:  
`filter`: only returns values in a sequence that matches the filter criteria.  
```
o = odds() #a generator excercise
f = filter(lambda x: x % 3 ==0, o)
```

"for each element in the o call f and then next", this is calling a *generator* acting on a *generator*.

`enumerate`: return a tuple for each element of the sequence of index and element data.  

`map`: applies a function to every element in a sequence and return a new sequence *lazily*, so each element is replaced.  
```
m = map(lambda x: x**2, odds())
next(m)
```

`accumulate`: keeps a running accumulation of applying a function to a sequence.```
def mul():
  return x*y

import itertools
a = itertools.accumulate(odds(),mul)
```



*Good for keeping running sums, also good for returning limits*

`chain`: takes a seqeunce of iterables and concatenates them. *This would probably be good for py4a.py* 

`zip`: get first character from multiple sequences and put them in their own mini sequence.  

**Iterable reduction function:** `max, min, sum, all, any`  

`sentinel` iteratively calls until a terminating element is returned.
```
def roll_dice():
  return randint(1,6)

i= iter(roll_dice,5)
```

> excercise: Use this for fizz buzz or something.  


## Coroutines
*generators* give values, *co-routines* take values. *(They can also return things)*  
```
def trivial_coroutine():
  while True:
    x = yield
    print("got a", x)

c = trivial_coroutine()
next(c)

c.send(4)
```
Running avg e.g.:
```
def running_average():
  total = 0
  count = 0
  average = None
  while True:
    term = yield average
    total += term
    count += 1
    average = total/count


averager = running_average()
next(averager)

averager(5)
averager(15)
```
Co-routines are sort of detached and maintain their own data.  
Normal program:
Main with tree of functions

Co routine program:
More like a graph, with functions calling themselves, more obscure to reason about.  
*Co-routines are slightly faster than objects*

This average could also be done with closures or classes.  
*This could be good for our dashboard*
## Abstract data types ADTs
**List ADT:** 
  * Sequence of 0 or more elements, can have more than one or more of a value. *(we are talking about abstract lists not python lists)*
  * First element is head, end element is tail
  * Have length
  * Elements have position  
  * Support:
    * Insertion
    * Deletion
    * Lookup  
**Linked lists:**
  * Have a Node keeping data for head of list
  * Second part of this Node has a link for where next Node can be found
  * There is a Node that has a link saying "you're at the end of the list".  
  * Visiting elements in a list is still order n.  
  * Inserting elements is efficient in that we just have to change the pointer of the element preceding it and give the element a pointer to the next element
**Doubly linked list:**
  * Each Node has a pointer to Node before and Node after.  

**LISP (list processing) language:**  
Demonstration of recursive list:
  * Using 2 element tuples as a Node, (value, pointer).  
  * First element is front of list, second element is rest of list.
  * To prepend l to y, we make a list (y,l)

> Exercise: Given an arbitrarily complex piece of code, how can you tell if the braces match? (Using stack).  

# Tock 5  
## Queue ADT
**What is a queue?** A queue is a type of stack, things *queue* at the back and *de queue* at the back.  
**Building a queue**   
* Give the queue a root.
* Give the queue a front and a back.  
* Going to use a doubly linked list to find nodes to either side.
* Our nodes have 3 elements, `data`, `previous`, and `next`, with `next` and `previous` being pointers.  
* For the first element "first", there is no previous or next.  BECAUSE there is no other element for it to point to.  
* The second element "second", now has `next` pointing to "first", with `previous` pointing to nothing.  
* The first element has `next` pointing to nothing and `previous` pointing to "second".  

**Deque** 
When we `deque` the "first" element we need to change the `front` pointer to "second" and make `previous` and `next` of "second" point to nothing.  

```
class Node():
  def __init__(self, previous=None, next=None, contents=None):
  self.previous = previous
  self.next = next
  self.contents = contents

  def __repr__():
  return "Node(contents={0}).format(self.contents)"

class Queue():


  def __init__():
    self.front=None
    self.rear=None


  def empty(self):
    return self.front==None


  def enqueue(self,datum):
    node=Node(contents=datum)

    if self.empty():
      self.front=node


    else: # make pointer to previous back of list
      node.next=self.rear
      node.next.previous = node

    self.rear = node

  def dequeue(self):
    
    
    if self.empty():
      return None

    contents = self.front.contents
    self.front = self.front.previous

    if self.front:
      self.front.next = None

    return contents

  def __iter__(self):
    
    
    n = self.rear
    
    
    while(n):
      yield n.contents
      n = n.next

  def __str__(self):
    return ', '.join([str(x) for x in self])
   
```
**Why use lists?** producer user problem: two lists that need to operate with each other. Can make one portion of a program to add to a list and another to take items off the list and operate on them.  
"The producer's job is to generate data, put it into the buffer, and start again. At the same time, the consumer is consuming the data (i.e., removing it from the buffer), one piece at a time. The problem is to make sure that the producer won't try to add data into the buffer if it's full and that the consumer won't try to remove data from an empty buffer."
## Queue implementation with linked list


## Collections.deque


## Set implementation with BST  
`or` function, returns a set that is a union of two sets. So `britis.__or__(french)` `british | french` returns a union set of cheeses that are in British or French. *For i in self add i and for j in other add j*
`and` function, return cheeses in BOTH sets. *for i in self, if i in other, add i*.  
`sub` function, if an element is in another set, remove it from the set. `for i in other delete i`.  
> Excercise: program this with the BST program already shown.  


# Lec NEW, Files  
## Python file methods
* Python file operations are designed to be portable. i.e. work with Windows and Mac and Linux. *This means it has concepts that are abstract and don't work for any specific OS*.  
* `append`, jumps to end of file and then writes. Important because the file could be being opened by many processes.
* `with ... as:` has defines local scope while something is the case, closes it after.  

### Buffering flag
* 0 unbuffered
* 1 line buffered *You force the file to read line at a time*.  
* other positive value, use buffer of  value size
*  -1 use system default
* `file_object.flush()` writes content of current buffer 
*Buffer is like a cache of info from or to the file.*  

## Linux System calls for File I/O
Works with `fd`, file descriptors. 
* `fd = open(pathname,mode)`
* `fd = creat(pathname, mode)`
* `bytes = read(fd, buffer, nbytes)`
* Linux doesn't see any structure in file, text and bin files are identical. They are just sequential bytes and structure is imposed at application level.
* Linux uses files for alot of stuff.  
*Can access the above functions, and more, through `os` package.*
Linux accesses files as system calls so they return errors that we can check if files have opened.  

## File I/O and Devices
* Hardware and devices are mapped to filesystem in linuc and manafed with open.read/write system calls.  
* ioctl takes fd command name and pointer to blob of data, look at.  

### Filesystems
HDD are just numbered blocks these days. With the PC having a register of the numbered blocks.  
If there is corruptions in a block the HDD finds a new block and renumbers it to be the old block.  
>Problem: How do we store files on a block device?   

Partitions in a disk: Takes a set of blocks and makes a subset into a partition.Super blocks: Knows meta data about how big a block is, where the blocks are and has inodes in an inode table.  
Directory: List of filenames with pointers to data and metadata.  

|-----------|-------------|----------|-----------|
|Super Block| Inode table | datablock| PARTITION2|
|-----------|-------------|----------|-----------|

### Directory structure
* Table of file name and inode (index node storing file metadata).   
* Name linked to inode with a link, `ln`.  
* Files can live in many directories, the tuple of name and inode uniquely identifies it. 
* inodes have info about timestamp, size.
* When we `rm` we unlink a file name and then when the file link count reaches zero the blocks used by the file are recycled.  
* We can open a file with a process, increasing the link count, then `rm` the file. Then when we close the process the file still exists but we can't find it.  

### Inode structure
Diagram  

### In memory tables
Look at

### Linux virtual file system (VFS)
ISO image: image format of CD ROM.  
* Abstraction layer that supports system calls for directory and file I/O.  
user space -->  VFS ------> filesystem ---------------> physical media
write()		sys_write() filesystem  write method	physical media
When a process begins it has 3 files, `stdin`, `stdout`, `stderr`.  
We can re route these as needed.   

### Pipes
can pipe output process of oen process into the input of another.  
* `os.pipe` returns two file descriptor, one for read one for write.  
* `fork` give readfd to end of child that reads and writefd to end of child that writes.
*Look at pipetest.py*  

> MS DOS used to use linked list of blocks, the end of each block points to the next block. This is an issue for wanting to seek to the end of the blocks.  


ProTip: Python only writes \>1 line at a time.  
ProTip: Also close the file when you're done.  
Protip: `ls -l` gives a number that is the number of links.  
Protip: each directory has at least one link.  
> Look at how disks are segmented.  
> Look at symbolic and hard links.  










bingo: cute, off piste, fasten your seatbelts, go forth
