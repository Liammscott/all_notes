# First class function
*in js we treat any function like a data type*  
```
const callback = () => 5;
const example = function(fn) {
fn();
};
example(callback);

```
# Pure functions  
```
const xs = [1,2,3,4,5];
//Pure
xs.slice(0,3); //[1,2,3]
xs.slice(0,3); //[1,2,3]
//Impure
xs.splice(0,3); //[1,2,3]
xs.splice(0,3); //[4,5]
```

```
//impure
let minimum = 21;
const checkAge = age => age >= minimum
//pure
const checkAge = (age) => {
const
}
```
> should favour pure functions. Separate impure effects away from pure.

## Pure functions: Array methods
```
const xs = [1,2,3,4,5];
xs.map(x => x+1);

```
> Pure function creates a new array.  
Map doesnt have to be an array, can be over any mappable Data structure. The DS is called a *functor*.  


Summary:
Use pure functions where possible
Dont create callback hell, use first class functions
Array (Stack, Queue), hash tables, functions, constants are best friends
Closures are powerful but slow.  
Always provide return statement.  

