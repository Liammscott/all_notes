# RECAP

```
//VALUES AND TYPES
typeof 3 === "number"
typeof 3.1 === "number"
typeof (() => {}) === 'object'
typeof 'hello world' === 'string'
typeof true === 'boolean'
```

```
//variable
const x = 3+3
let y = 'hello world'
```
Difference: using `const` cannot change the variable.
const x = 3
let x = 2
```
const xs = [];
xs.push(3);
```
in the above case, array has been mutated but is still array.  

## functions
**Function expression**  
```
const f = function(x,y){
    return x+y
};
```
**Function declaration**  
```
function g(z,k){
    return z-k
};
```
DIFFERENCE: When compiling JS looks for all declarations and puts them at the top of the page. *Hoisting*
So
```
g(1,2)
function g(z,k){
    ...
}
```
is valid.
but
```
f(5,7)
const f = function (x,y){
    ...
}
```
is not!

more complex:
```
const h = function(){
    return f(7,5)
}
const f = function(x,y){
    ...
}
f(5,7);
h()
```
is also valid!


## Writing function expression
```
const h = function(){
    return f(x,y)
}
const h = () => {
    return f(x,y)
};
const h = () => f(x,y)
```

All mean the same thing, should use the top method to be safe.  

## Conditionals
```
if (1+1 === 2){
    // block
    const x =1;
}
else if (2-1 < 3){
    const y=2;
}
else{

}
const x = 'hello world';
switch (x){
    case 'hello world':
    // lines
    break;
    default:
    //lines
    break
}
```

> The 3 = signs: 1 == '1' returns true, === checks type as well as value. Should always use === in JS.

## Fibonacci
```
const fib = (n) => {
    if (n<=1) return 1;
    return fib(n-2)+fib(n-1);
}
```

# 06/12/18
## Window  of the global object  
Using const and let we put variables in block/local scope.  
This is good because we don't pollute the global scope/window.  

## Loops
Simplest loop:
```
for (let i = 0; i < 0; i++ ){
    console.log('hello world')
}
```

Less low level:
The `in` operator
```
const person = {
    name: 'Jason Yu',
    age:23,
    work:'thg',

};
const jobless =! ('work' in person);
```
The `for-in`
```
for (const key in person){
    console.log(key, person[key])
}
```
> An object in JS is a map from key to object.  

Since an array is also an object in JS we can use `for-in` with arrays:
```
const xs = ['a', 'b', 'c']
for const(key in xs){
    console.log(key, xs[key])
}
```

Can do `for-of` loop:
```
const ys = ['mary', 'jame', 'butter']
for const(y of ys){
    console.log(y)
}
```
use `for-in` for objects, `for-of` for arrays/iterables.  
> Object.entries(obj) creates an iterable list of the objects properties

## Excercise
**factorial**
```
function factorial(n){
  let result = 1;
  if(n===0) return result;
  else{  for(let i=1; i <= n; i++){
      result=result*i
  }
  return result;
  }



}
```

**Fancy recursion**
```
[n, n-1, n-2,...,1]reduce(*)
```
This reduce replaces commas in the list with the sign/function supplied as an argument to reduce.  
So we can write:

```
Array.from({length:10}).map()(x,i)=>i+1)

const FactFancy = (n) => {
    return Array.from({length:n}).map()(x,i) => i+1).reduce((x,y) => x*y);
};
```


what is node?
