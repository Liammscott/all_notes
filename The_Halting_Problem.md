# The Halting Problem  
> computability: Ability to solve a problem in an efective manner.  

Turing thought of, and described, the Turing machine to solve to Halting Problem.  

**The Church-Turing Thesis**  
All funcitons you can computre with a turing machine is exactly the same functions you can compute with any general method.  
This is super powerful you can come up with differnt definitions for solving problems and then you can relate them back to the Turing machine, then you know itsidentical to other definitions.
**Turings Turing machine and Alonzo Church's Lambda calculus ARE THE SAME **

> If we never invented programming languages, the methods we used to solve a program would be the same.  

## The halting problem  
> Will a program run forever  
The Collatz conjecture: take a pos int, if even /2 if odd *3. Does this terminate at 1?  
This is a HARD problem, not solved in maths yet.  

Bear in mind:  A positive solution to the Halting ptoblem itself must be a program that does not run forever. It has to take a finite number of steps and return:
	1 if progrm P given date D will terminate
	0 if not 

## Some intuitions  
Fascinating: because P and D are FINITE. All the data can be fitted on paper.  
The answer is in the code, how do I know it will DEFINITELY terminate?  
Could run for 10, might terminate on 11th, 100 might terminate on the 101st, cannot know if it will terminate until it does!  

```
def halting(program,data):
	if terminate:
		return 1
	else:
		return 0

def tricky(p):
	t = halting(p,p)
	if t == 0:
		return 0
	else:
		while True:
			pass
```

`tricky` is a valid function, and will run PROVIDED halting is a valid function

Now, take tricky and apply to self, gives t = halting(tricky,tricky)  
so, t can be 1 or 0.
IF: t =1, tricky terminates!



Proof by contradiction!  

## What is possible?

If someone ever writes a program that does the halting problem, you can construct 6 lines in python to break it.  
Not to give up hope, you can find situations where a program halts, but there isno proof. YOU CANNOT GUARANTEE YOUR PROGRAM WON'T HALT GIVEN SOME SPECIFIC INPUT.  
## Some tricks
If we define no for, no recursion (simply typed lambda calculus), we avoid this.
BUT these languages are not Turing complete.  


> Is the turing problem based on how our computation works? would quantum computers have the same problems? how do you know that there aren't other logics we don't account for?  

