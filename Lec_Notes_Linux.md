# Lec1
## Definitions
**OS:** Made out of *software* to manage *hardware*. This provides **Abstraction**.
**API**: Application Programming Interface, functions you can call as a programmer that expose the underlying structure of the PC.

## Examples of what the OS manages
* Low level operations, connectivity, cpu, memory, network card etc.
* Runs software/processes, manages and schedules them to avoid collisions.
* Manages users, security, and rights.


##

## Course Goals (what is expected of me)
>1. Be a comfortable/sophisticated Linux user
>2. Learn how operating system entities work, and how to program them with system APIs.
>3. Extract some general principles (managing concurrency/distributed computing for example)

## OS components
* **Kernel:** manage hardware, present abstractions and system call API
* **Libraries:** Higher level interface system calls.
* **Shell:** Convenient user interaction. 


## Some useful commands 
scp -P 2222 filename vagrant@127.0.0.1:~/  

scp -P 2222 vagrant@127.0.0.1:/path /to/filename localname  


curl https://URL,	collects file spits it out into terminal  

curl https://URL > .txt,	collects and spits file into a .txt  

cURL: “ClientURL”  

# Lec 2
## Command line arguments
`#!/usr/bin/env python `
`import sys `
```
#command line args in a list of strings called sys.argv, first element is name of program as envoked, remainder are comman line args
   ```
**Standard input:**  This is the file handle that your process reads to get information from you.  

**Standard output:** your process writes normal information to this file handle.  

**Standard error:** your process writes error information to this file handle.




# Lec 4
*Recall: 1 byte can represent 256 values/characters*
Today:  
*
* Some reflections on strings.
* Information density: byte represents 256 values
* Representation tradeoffs: our python (CPython). Given a string there are different ways to pack it (UTF-8 for small characters, UTF-16 for larger characters).  
                                  * Python internally represents strings as arrays. Jump to nth space/element/character by looking 8 memory spaces on.
                                  * Python samples the characters in a string and finds the most efficient way to represent string (16 or 8 bit), then stores metadata for this so it knows how to iterate.
                                  * *Remember 1st character, s[0], has memory space= to the memory address of the string*
                                  * *Tuple, each element is the size of some object that can point to where an object is stored*
    * Numerical encodings for integer and floating point
    * Model computer
    * File I/O.  
  
## Numbers
`int` are typically 32bit or 64bit. *64bit can store 2^64 numbers. BUT if consider we need to also store -ve numbers, we use the highest order to say if the number is -ve or +ve*  
**Two's compliment**
Consider 8bit: largest number is 0b11111111. If we add 1, 0b1, we get 100000000, which in 1byte representation is 00000000, the smallest number expressible in 8bit.  
*Computers tend to keep track of any "overflow bits" but can't do further maths*
Consider 8bit signed: Same result, but would flip to being a -ve number.
*How do we interpret the other 7bits if the 1st bit is a 1, meaning a negative number?*
NOT: -ve whatever the other 7bits are.  
We take the compliment, flip all bits, and add 1 to it, giving the positive value.
*Interesting maths:* can subtract by adding negative number

**Floating point numbers**  
* Scientific notation
* mantissa \* (base \* \* exponent)
* Normal form *the 1<mantissa<10*  
  
> Mantissa represented by IEEE-754. We know that there needs to be a signed bit

|1 signed bit| 52 mantissa bits | 1 exponent signed | bit 10 exponent bits|

*With the first mantissa bit=1 to imply that it's normalised, thats 51 bits to represent the decimal part*
2\*2\*\*(2**10)=4\*\*1024~=10\*\*308
This number is much more than the largest int expressible with 64bits
*Note: you cant convert big floating points into regular int. The system being used needs specialised maths to handle this.*
*e.g. (1E20-1E20)+1=1, but (1E20-(1E20+1)=0)* This suggests 1E20+1 and 1E20 are represented by the same 64bit pattern.  
Python uses `big num` for precise arithmetic, arrays of pointers to ints (similar to tuple??) this is an **abstraction**
*if we are willing to have more approximate numbers we get to have larger numbers*
* at their extremes floating points are *lossy*  


## Something more practical
can we, in python, make a more lossless representation of big numbers.  
a cloud of data, containing numerators and denominators, and a function `make_rat()`.  
We can imagine:
``` 
    make_rat(numerator,denominator):
        return ((numerator,denominator))
   ```
``` 
    numer(rat):
      return rat[0]
   ```
``` 
    denom(rat):
      return rat[1]
   ```
``` 
    rat_add(rat1,rat2):
      num= numer(rat1)*denom(rat2)+numer(rat2)*denom(rat1)
      den=denom(rat1)*denom(rat2)
      return (num,den)
   ```
```
   str_rat(rat1,rat2):
      return "{0} / {1}".format(rat1,rat2)
   ```

