# What are IPs and Ports
**Example:** `0.0.0.1:8080` is `IP address:port`
> Analogy: IP address us main office telephone, Port is your extension.  
The IP address alone isn't enough to access a server, need the port number.  
We could be running multiple processes on the same IP address (machine) on different port numbers, so we need to include the port number with our request so can access the service.


