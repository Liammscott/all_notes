#Why
Efficient, highly scalable, dig deeply!
Last decade, clusters, networking demands more efficient computing.  
Made by google, efficient and scalable.  

## File structure
Keep all code in a single workspace
`src` contains Go source files
`bin` contains executable commands  
`go` tool builds and installs binaries into the `bin` directory. 
`src` typically contains the multiple version control repositoreis (like git) that track dev of one or more source pages.  

### creating workspace
`go env` give workspace info  

in directory `/Users/scottl/go`
```
mkdir go
cd go
mkdir src bin
cd src
mkdir github.com
cd github.com
mkdir liammscott #This is where projects will go
```

## Installing with `go get`
```
go get github.URL/Url
```
* You should install aws, gives core SDK utilities and shared types. use this packages utilities to simplify settings and reading API operational parameters (This is dense)

so in this e.g.
```
go get github.com/aws/aws-sdk-go/aws
```

## Code structure
make directory e.g. `01_hello` and in here make `main.go`

> In `go` there is a main function that runs automatically

## Create and install binaries
`go install` compiles and creates an executable.
Can then go to the `bin` directory and run the code with `./01_hello`  


## Creating variables
```
var name string = "Brad"
var name = "Brad"
var age int = 37
var age = 37
```
All work and are equivalent.
>If you create variables and don't use them you get an error.

`%T` gets type of variable.  
e.g.
```
fmt.Printf("%T\n", age)
```

Short hand notation is `name:="Brad"`
for efficiency `name, email := "Brad", "brad@gmail.com"`

## Installing external libraries
Install external libraries with the package manager tool.  
Package manager for go is dep.


# Passing by value
This is what is really special about Go.  
Everytime a variable is passed as a parameter, a copy of the variable is created and passed to a called function or method.  
The copy lives at a new memory address.  

So, when i call a method on a variable, the method modifies a copy of the variable, not the variable itself.  

If call a method on a pointer to a variable it does change the variable, becausea copy of the value (a pointer in this case) is made. this pointer still points to the original variable and so does cahnge it. 

In go, unlike in C, passing by value is actually way cheaper than passing by pointers. 


# Interfaces and types
```
type Human struct {
        name string
        age int
        phone string
    }
    func (h *Human) SayHi() {
        fmt.Printf("Hi, I am %s you can call me on %s\n", h.name, h.phone)
    }
    func (h *Human) Sing(lyrics string) {
        fmt.Println("La la, la la la, la la la la la...", lyrics)
    }
    type Men interface {
        SayHi()
        Sing(string)
    }
```
> "Hey baby, if you have these methods, then you're my type" - Interface

# net/http package


* `ServeMux` is a HTTP request router (Multiplexor). Compares incoming requests against a list of predefined URL paths and calls the associated `Handler` for the path when a match is found.

* `ServeHTTP` dispatches requests to the handler whose pattern most closely matches the request URL.

* `Handlers` are responsible for writing response headers and bodies. Any object can be of *type* handler if it satisfies the `http.Handler` interface,

* `Handle` registers the handler function for the given pattern in DefaultServeMux.

```
type Handler interface{
	ServeHTTP(ResponseWriter, *Request)
}
```


```
func Handle(pattern string, handler Handler)
```
> Note: `ServeMux` has a `ServeHTTP` method, making it a kind of handler. The only thing it does differently is pass the request onto a second handler. (This is a common practice in Go)

### Example


```
package main

import (
  "log"
  "net/http"
)

func main() {
  mux := http.NewServeMux()

  rh := http.RedirectHandler("http://example.org", 307)
  mux.Handle("/foo", rh)

  log.Println("Listening...")
  http.ListenAndServe(":3000", mux)
}
```
In the above example, `http.NewServeMux()` is a function to create an empty ServeMux (Which compares incoming requests against a set of URLs and calls the handler found for the most appropriate).  
`http.RedirectHandler` function replies to a request with a redirect to a url based on response code.    
`mux.Handle` registers `rh:= RedirectHandler` with new `ServeMux`, acting as a handler for for all incoming requests with URL path `/foo`  
`http.ListenAndServe` creates and creates a new server and listens for incoming requests, passing in our `ServeMux` for it to match requests against.

## Custom Handlers
**Example:** A custom handler tht responds with the current local time in a given format:
```
package main

import (
  "log"
  "net/http"
  "time"
)

type timeHandler struct {
  format string
}

func (th *timeHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
  tm := time.Now().Format(th.format)
  w.Write([]byte("The time is: " + tm))
}

func main() {
  mux := http.NewServeMux()

  th := &timeHandler{format: time.RFC1123}
  mux.Handle("/time", th)

  log.Println("Listening...")
  http.ListenAndServe(":3000", mux)
}
```
In `main` we initialised `timeHandler` in the save way we would a normal `struct`. Using `&` to yield a pointer.  Then use `mux.Handle` function to register this with ServeMux.  
ServeMux now passes any requeset for `/time` to our `timeHandler.ServeHTTP` method.  

We could also reuse `timeHandler` in multiple routes:
```
func main() {
  mux := http.NewServeMux()

  th1123 := &timeHandler{format: time.RFC1123}
  mux.Handle("/time/rfc1123", th1123)

  th3339 := &timeHandler{format: time.RFC3339}
  mux.Handle("/time/rfc3339", th3339)

  log.Println("Listening...")
  http.ListenAndServe(":3000", mux)
}
```
## Functions as Handlers
For simple cases, defining custom types and ServeHTTP feels OTT. Alternatively, we leverage Go's http.HandlerFunc type to coerce a normal function into satisfying the Handler interface.  
Any function with the signature func(http.ResponseWriter, *http.Request) can be HandlerFunc type. Usefule because HandleFunc objects come with inbuilt `ServeHTTP` method which - rather cleverly and conveniently- executes the content of the original function. 

Converting a function to HandlerFunc and adding it to ServeMux is so common, Go has a shortcut: `mux.HandleFunc`
```
func main() {
  mux := http.NewServeMux()

  mux.HandleFunc("/time", timeHandler)

  log.Println("Listening...")
  http.ListenAndServe(":3000", mux)
}
```

The next option is putting out handler logic into a closure, and close over the variables we want to use: 
```
func timeHandler(format string) http.Handler {
  fn := func(w http.ResponseWriter, r *http.Request) {
    tm := time.Now().Format(format)
    w.Write([]byte("The time is: " + tm))
  }
  return http.HandlerFunc(fn)
}
```
this function returns a handler. It creates anonymous `fn` function which accesses/closes over the `format` variable.   
Importantly the closure has signature `func(http.ResponseWriter, *http.Request)`, meaning it can be converted to handler type.  

Other formats for this short hand are: 
```
func timeHandler(format string) http.Handler {
  return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
    tm := time.Now().Format(format)
    w.Write([]byte("The time is: " + tm))
  })
}
```
or implicit conversion to HandlerFunc:
```
func timeHandler(format string) http.HandlerFunc {
  return func(w http.ResponseWriter, r *http.Request) {
    tm := time.Now().Format(format)
    w.Write([]byte("The time is: " + tm))
  }
```

## The DefaultServeMux
`var DefaultServeMux = NewServeMux`
from the go source code.
> Generally: avoid `DefaultServeMux` as it poses a security risk. It is stored in a global variable, any package has access to it. Including any imported 3rd party app. It is better to use your own locally-scoped-Mux.  

Finally, `ListenAndServe` will fall back to `DefaultMux` if no other handler is provided (if the second parameter is set to nil).  

## Gorilla secure cookies
HMAC, hash based message authentication code.
*If someone is watching the https connection, using wireshark, they can find the uuid for the cookie and make their own cookie with that uuid and hijack the session, HMAC is to fix this*
Gorilla sessions pre-built library helps with this.

Questions:
> Closures (this feels like more of a general question)
> What is a switch
> What is a slice
> Arrays in go 
> Implementation of go
> go syntax


