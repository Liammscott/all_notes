## Good software design takes the pain out of testing
Single responsibility priciple
Inject dependencies into classes
Avoid global variables
D.R.Y

## Getting the level of coupling right
Try to get the code and test to fit together just right.  

>Note: There is a third argument in the assertEqual/assertTrue function that returns human readable code.
>Note: You should use helper functions in the test file.  

Weak test: doesn't test the fullest capabilty of the code.
Good test: No repetition, test full capabilty of the code.


## Drawing out a structure

```
with self.assertRaises():
    exception
```

## Code coverage
~80% is a good level for commercial level.  
PyCharm: right click, 'Run with coverage' runs tests, prints output, reports on coverage.  
