#Databases
# SCHEMA

## Levels of abstraction  
* Physical/internal
* Conceptuak/logical level
* View/external level
Most DBMSs do not separate the three levels completely and explicitly, but they agree with the architecture to some extent.  


## Entity-Relationship Model  
Models an enterprise as a collection of *entities* and *relationships*.  
* **entity:** a thing or object distinguishable from other objects.  
* **relationship:** association between several entities.  

**relationship** and **entities** can both have **attributes**.  
*Consider: a student and advisor **entities** have instructor **relationship** with the start date **attribute**.*  


## Binary and tenary relationships  
OFFERS during a SEMESTER of a COURSE  by an INSTRUCTOR is a **tenary** relationship.  
*tenary relationships are good in that they can cut down on number of additional links needed as they can be inferred*.  

**Composite attributes:** NAME: First_name, middle_initial, Last name. with each of these composite attributes being a simple attribute that can't be broken down further.  
**Derived attributes:** Can be found from other data points (AGE from DATE OF BIRTH and CURRENT DATE).  
**Single valued and multivalued attributes:** AGE OF PERSON is **single valued**, COLLEGE DEGREES (can be) **multivalued**.  

## NULL values  
Represents:  
* Unknown, missing values due to human error
* Truly Unknown
* Specific value for a needed field that is as yet undefined.  

Consider:
**unknown** home phone number of John Smith  vs **not applicable**, college degrees are applicable only to those with college degrees.  


## Constraints
### Keys  
* **super key** of an entity is sey of one or more attributes that uniquely define an entity.  
* **candidate key** of an entity set is a super minimal key.  *ID is a candidate key of instrucctor entity.* *course_id is a candidate key of course entity.*  

Several candidate keys exist, the simplest is chosen as primary key.  


*Can do ER or DB schema diagrams*  

## More schema based constraints  
> Constraints are derived from rules in the miniworld that the databbase represents.  

* **Entity integrity:** constraint states that no primary key value can be NULL.
* **Referential integrity:** constraint and foreign keys is specified between two relations.  


## Cardinality ratio and participation constraints  
**cardinality ratio** is the maximum number of relationshp instances an entity can participat in, for a binary relationship.  
**participation constraint** specifies if the existence depends on its relationship to another entity. (The entities are then a weak and strong entity.)  
**Total participation** and **Partial participation**

**One to many relationship:** Advisor can have multiple or no students.
**Many to many:** Instructor with multiple students through advisor and students with multiple instructors through advisor.  

## Normalisation  
**Normalisation** is the process of decomposing large tables down into smaller tables to remove redundancy.  
*Typically a table should serve only one purpose. we call this **the first normal form**.*
**First Normal form:**
  * table stores info in rows and columns with one or more columns being the primary key.  
  * each column contains atomic values, which cannot be subdivided.  
  * no repeated rows or columns.  
  * no sub columns, e.g. a list of cities in a column.  
*Can have a foreign key to FK table, where no pattern of FK to FK is repeated.*
> Excercise: Issue with cars having multiple owners.  


**Second Normal form:**  

# Lec 2
## Weak Entity
*Entity* types that do not have their own **Key attributes**.  
The existence of the weak entity depends on the existence of the **Primary entity**.  
*It must relate to the identifying entity set via a **total, one to many** relationship set from the identifying to the weak entity set*  
The **discriminator/partial key** of a weak entity set is the set of attributes that distiguishes between all weak entities.  
The **PK** of a **entity** is formed by the primary key of the dependent stron entity set and also the weak entities discriminator.  

## Normalisation  
**Normalisation** is the process of decomposing large tables into smaller tables.  
*This eliminates redundancy, and errors that arise with data removal and updating*  
**Normal form:** A set of rules that can be used to test a table structure to ensure that it is sound and free of problems.  
**Anomalies:**
  * Insertion anomaly: some data has to be inserted with a lot of NULL values.
  * Update anomaly: Multiple updates with inconsistencies.
  * Deletion Anomaly: More than one set of facts removed.
  * Search and sort: Need to search multiple columns to query one fact.  


**Normalisation solution:** Break down, each table does only ONE thing, use FKs to link tables of different purposes.  

**1st form:** No repeated sub columns, no repeating groups of columns, column contains atomic values. One or more columns as PK which *uniquely identify* each row.
**2nd form:** Narrow each table to a single purpose. All non-key columns are dependent on the table's PK.
**3rd form:** Table only contains columns that are non-transitively dependent on the PK. *These relations can give update anomalies*.  
**Transitive Dependence:** columns value depends on another column through another transitory column. *Make another table to subdivide these.*  


>Intersection table: Many to many cardinality gives a lookup table/intersection table of just keys.  

*Pro tip: Look at a row level to find duplication and issues to fix.*  

## ER to Relational Mapping
**Mapping procedure:**
  1. **Map regular entities**
  2. **Map weak entities**, create a relation for each and include all simple attributes and the FK.
  3. **Map binary 1:1 relationship**, there are 3 approaches for this:
    * FK approach (most common): Identify relations that correspond to entities participating in R. Entity with *Total participation* has the foreign key of the other entity.
    * Merged relation approach: Merge the two entities and the relation into a single relation.  
    * Cross reference approach: Create a third
  4. **Map of binary 1:N relationship**, there are two possible approaches for this:
    * FK approach
    * Cross reference approach
  5. **Map of binary M:N relationship**, cross reference approach.  
  6. **Map of multivalued attributes** Create a new relation for each.
Map regular entities, department, employee, project. THEN map weak entities, create a relation table and for each include all simple attributes and FK
THEN map binary 1:1 relationship.
  * 3 possible approaches:
    * FK approach (Most useful unless special conditions exist): Look at full participation entity, then put the FK for the entity it participates in. (Putting the other way round gives alot of NULL values)
    * Merged relation approach.
    * Cross reference approach.
THEN map of binary 1:N relationship. FK approach: on the N side put the FK of the 1 side in the entity of the N side.  
For recursive relationship:  
THEN Map M:N relationship, cross ref approach.  
THEN Map multivalued relation: Make a subtable for multivalued attribute.  

THEN, on our table diagram we draw arrows linking the FKs to PKs.  

## Mapping of an N-ary Relationship Type  
Make the relationship an entity, then map in a similar wat to M:N.  

## UML class diagram notation: Company example  
Can include operations in these diagrams.  
Represent relationships with their own attributes as entities.  
Represent multi valued as own entity.  

> Excercises on git, deadline Monday. Make relational map, try make SQL table. Add completion sheet. Crappy table = Crappy queries.  

# FEEDBACK ON ERD
*For weak entity need to look at the total participation to identify PARENT STRONG ENTITY*  

# Lec 3
## The DB language, SQL  
* SQL is a *very* high level language, we say "what to do" not "how to do it".  
  * This avoids a lot of data-manipulation details needed in procedural languages like C++ or Java.  
* **DDL:** data definition language.
* **DML:** data manipulation language.  

* **Interactive SQL:** get immediate results with ad hoc statements in terminal
* **Embedded SQL:** SQL in host program.


## DML
*Language for accessing and manipulating the data organised by the most appropriate data model.*  
Also known as *query language* DML allows query, insertion, and deletion of data.  
We can query on one or more **relations** with sub queries and related queries.

## The select clause
* SQL allows for duplication in relations and queries. Can use `distinct` to remove duplicates from queries.  
* An `*` in the query denotes *all data types*.  


## The where clause
`where` clause denotes the conditions the query must satisfy. Conditions can be `and` & `or`.  
*Note: we can use the same names across tables.*  

can set aliases
```
from employee e, department d
select e.fname where e.dnum = d.dno
```
*Note: in this example have are 'dnum' and 'dno' linked by a pointer?*  

## Examples: Basic queries  
```
SELECT BDATE, SALARY
FROM EMPLOYEE
WHERE FNAME="JAMES" AND LNAME="ANDREWS"
```
More generally
```
SELECT the_attributes you want
FROM the_table_you're_interested_in
WHERE these_conditions_apply
GROUP BY <attributes> HAVING <conditions>
ORDER BY <list of attributes> ASC/DESC
```
We use **alias** *before* defining it! This is okay.  
*We use primary key = foreign key NOT the other way round*

## Nulls
It is possible to have tuples with NULL value.
Causes:
  * Missing values/unknown
  * Inapplicable
Rules:
  * Arithmetic with NULL returns NULL
  * Can query for NULL

# Lec 4

## SQL statements
*Data manipulation language*  

(Look at slides)
```
select
Update
Insertion
delete
MERGE
```
## Column aliases
```
SELECT last_name "name", salary*12 as "Annual salary"
```
use "" for case sensitive!

## Concatenation
```
SELECT CONCAT ('FirstName','LastName') AS 'Full Name'
```

## Relational Operators
(Slides)
*Restriction* selects rows from a relation that meets certain conditions. This is a 'horizontal subset'
*Projection* This is an operation that only selects specified columns from a relation. This is a 'vertical subset'
*Product* The product operation is the result of selecting rows from

## Arithmetic expressions
* MySQL allows for operational preference.  
* Arithmetic operations containing a NULL evaluate to NULL.  

## Comparison and logical Operators
BETWEEN .. AND.. (Includes <= >=)
IN
like
IS NULL

Can restrict rows with where, comparison conditions, and logical conditions.  


SELECT NI
from employee
WHERE (DN0 = 3 AND salary*12 => 10,000) OR (DNO = 5 AND salary*12 => 15,000)

Logical precedence: Will perform AND before OR
List of precendence on SLIDES


## Group functions  
Many rows in, one Output
e.g. AVG, COUNT, MAX, MIN, STDDEV, SUM, VARIANCE  
**Syntax**
```
SELECT group_function(column)
From table
```

```
select count(*)
from EMPLOYEE
where dno = 3
```

## Grouping Rows
`group by`
SELECT column, group_function(column)
FROM table
WHERE conditions
GROUP BY group_by_expression
ORDER BY column

SELECT dno, AVG(salary)
FROM EMPLOYEE
GROUP BY dno

Note: GROUP BY *doesn't* have to be in the SELECT list. (Must be in a table from the FROM list)  
Note: Need to use HAVING clause to restrict groups.  

SELECT max(sal)
FROM (SELECT dno, sum(salary) sal
FROM employee e
GROUP BY dno) ee;

Note: Need to use max(sal) not max(salary) to ensure we are querying our subtable we made. Also note the ee alias for our subtable.

## Data definition language
Create table contruct:  

Domain data types in SQL

Integrity violation options  
can RESTRICT (reject deletion/update)
can CASCADE (propagate the deletion/update)
can SET NULL or SET DEFAULT

## DML
INSERT INTO  

# Lec 5: Querying more than one table  

## Join semantics  
**Cartesian Product:** instructor X teaches, returns alot of overlap. If we DON'T input conditions on results then cartesian product is usually used.  
* Join operations take two relations and return it as a result of another operation.  
* Join operation is a cartesian product that requires two tuples in each of the two tables match.  

**Natural join:**
	* The natural join clause is based on all columns in the two tables that have the same name and retains only one copy of each common column.  
	* It selects rows from the two tables that have equal values in all matched columns.
```
SELECT *
FROM Instructors NATURAL JOIN Teaches
```

**Why combine data?**  
* Normalisaition reduces each able to one purpose.
* Join stitches the tables back together to answer most of the questions we ask the database.  
* **Mechanics of the join:** like the overlap of two regions on a venn diagram (this is an **inner** or **natural join**)  

**Types of join**
* *Cross joins* return all combinations of rows from each tabe no koin conditions are required,
* *Inner joins* return rows where the koind confition is met/true. In its most common example where a primary key is used.
* *Outer joins* are usedd whemn in adition ro returning the reulst of an inner join you want to return rows from tables that dont match each other.  

**To avoid cross joins always use valid conditions**

```
 select * from instructor natual join student
```
returned nothing
```
select * from instructor cross join student
```
returned all results

**Outer Join**
An extension of join tht avoids infrmation loss.  
Computes join and then adds tuples from one relation not matching tuples of the other relation to the result.
* *Left outer join:* retrieve inner join + left relation. (retrieve records left table plus those from the right table matching the join) (Venn diagram intersect and left circle returned)
* *Right outer join:* retrieve all records from table B plus records from table A which match the join.

* It uses the NATURAL JOIN criteria if not otherwise specified. (Just returns rows with some matching data).  
**Outer join will do its best and return a NULL where information cannot be found**  
> BIG NOTE: Join will work even if we don't use primary keys! So you should name your columns accordingly!  

**Joining table syntax**

```
Select table1.column, table2.column
from table 1
natural join table 2 | join table2
CHECK THIS
```

**Creating Joins with the USING clause**  
If several joins have the same names but non matching data tupes, use USING to specify columns for the join.  
You can only specify one column in USING.

```
select name, title
from instructor natural join teaches
join course using(course_id)  
```

**Joins return *temporary views***  

**USING vs. ON clauses**  
* USING is used if several columns share the same nsame but you don't want to join using all of these common columns.  
* ON is used to join tables where the column names don;t match in both tables.  
* The ON clause preserves the columns from each joined table separately, where the USING clause merges the columns from the joined tables into one column.

**Again as with USING we can only use one column.**

## Few remarks  
* Be aware of unrelated attributes with the same name which get equatedd incorrectly.

## Set attributes
**Implicit and Explicit join**  
```
from instructor natural join teaches
OR
from instructor inner join teaches on instructor.id = teaches.id
```

# Lec 6
## Self join
*Join a table to itself*  

e.g. display a list of customers who are in the same city

```
SELECT c1.city, c1.customer_name, c2.customer_name;
FROM customers c1 INNER JOIN customers c2 ON c1.city = c2.city AND c1.customername > c2.customername;
ORDER BY c1.city;
```
## Set operations
*union*, combine two or more sets of queries into a single result set  
*intersect*, returns only distinct rows of two queries or more
*except*, CHECK  
Each automatically remove duplicates.  
e.g.
```
select course_id from section where semester = "fall" and year = 2009
union
select course_id from section where semester = "spring" and year = 2010
```

**Rules for set operators**
Set operations have the following restrictions:
1. Column names
2. Order by clause only in final query set statements3.
3. Group by and having only in individual queries. (Cannot use them to affect result)

**Precedence order**
UNION and EXCEPT have same order
INTERSECT has higher precedence

**Joins vs. Unions**  
Joins combine data into new columns where as Union combines data into new rows.  

**Imitating Instersect in MSQL with INNER JOIN**
```
SELECT DISTINCT id
FROM t1
INNER JOIN t2 USING(id)
```

**Except and Minus**
```
SELECT id FROM t1
MINUS
SELECT id FROM t2
```
```
SELECT id
FROM t1
LEFT JOIN t2 USING (id)
WHERE t2.id IS NULL
```
# Lec 7
## Order of execution of query
1. `FROM` and `JOIN`
2. `WHERE`
3. `GROUP BY`
4. `HAVING`
5. `SELECT`
6. `DISTINCT`
7. `ORDER BY`
Note: Need to be careful that `JOIN` only returns one column or need to alias our tables.  

Consider: `SELECT` is processed last and it can be helpful to think about this last.  


## Subqueries/Complex queries
A **Subquery** is a `SELECT-FROM-WHERE` nested in another query.  

Consider:
```
Select A1,A2,...,AN
FROM r1,r2...,rN
Where P
```

Ai can be replaced by a subquery of 1 return value.  

FINISH

A subquery must *always* be in parentheses.  

```
SELECT * FROM t1 WHERE column1 = (SELECT column1 FROM T2)
```
e.g. delete instructors with less than avg salary

```
delete FROM instructor
where salary < (select avg(salary) from instructor)
```
issue: avg changes
Solution: Read

## Evidence test
Using `WHERE` we can check `IN` to test whether the column value in the current row existed in a fixed list of values.  
In COMPLEX QUERIES THIS CAN BE EXTENDED TO USING SUBQUERIES TO CHECK IN DYNAMIC VALUES.

**Membership and comparison tests**

**Membership:** Useful to read these subqueries inside out, uses `IN` in a subquery.  
**Comparison test:** Uses a `SOME` in subquery.  

## Comparison clause definitions
### Definition of `SOME` clause
### Definition of `ALL` clause
### Definition of `ANY` clause  
Note: `ANY` and `IN` often return the same results, should look at their underlying logic to see comparison.  

## Correlated Subquery
The inner query references, or is dependent on, an aliased table from the outer query.  

Note: `JOIN` preferred to `WHERE` as it is passed first and so is easier to optimise.  

## Scoping rule
Evaluation is done from inside to outside.  

## Null values and Three valued Logic
Comparison of `NULL` returns unknown.  
Booleans with `OR` `AND` `NOT` including `NULL` can return `TRUE` `FALSE` or `UNKNOWN` depending on context.  

## Example queries
### `IN` and `ALL`
1. Total number of distinct students taken courses taught by instructor ID 10101
```
select count (Distinct ID)
from takes
where (course_id, sec_id,year) in (select course_id,sec_id, semester, year
	from teaches
	where teaches.ID = 10101
);
```
> There is a simpler way

2. Find employees whose salary is greater than the salary of all the employees in dept 5
```
select name
from employee
where salary > ALL ( select salary from employee
	where DNO=5)
```
3. Find the name of employee who has a dependent with same f and l name and gender as employee
```
SELECT E.FNAME, E.LNAME
FROM EMPLOYEE E
WHERE E.ni IN (SELECT superni
	FROM DEPENDENT
	WHERE superni=E.ni AND 
	E.FNAME=DEPENDENT_NAME AND SEX=E.SEX
)
```
### `EXISTS`
Retrieve employees with no dependents
```
SELECT FNAM, LNAME
FROM EMPLOYEE
WHERE NOT EXISTS (SELECT* FROM DEPENDENT 
	WHERE NI=superNI
	)
```

List names of managers who have at least one dependent
```
SELECT FNAME,LNAME
FROM EMPLOYEE
WHERE EXISTS (SELECT * FROM DEPENDENT 
	WHHERE NI=SUPERNI
	AND
	EXISTS(SELECT *
		FROM DEPARTMENT
		WHERE NI=MGRNI
)

Find the enrolment of each section that was offered in Autumn 2009
```
SELECT
FROM
WHERE
```
)
```

# Lec 7, Query Optimisation and Indexes
## What are indexes
* **Indexes** sort data in an organised sequential way. Like an alphabetically sorted list. 
* We use them as a method of optimising databases, as this is a better way of improving DB manipulation than just buying more hardware.  
* Looping through database of randomly stored entries is slower than looping through sorted data.  
* Indexes are created on columns that will ne used to filter the data. 
**NOTE:** indexes on columns that are frequentky updated can result in poor performance. We should use them on columns which aren't changed ut are used to query often.  
  
## Create Index Syntax
```
lookup
```
## Index vs. Primary key
* PK is a logical object which uniquely identify an entity/table, SO pok CAN BE USED TO IDENTIFY A SINGLE ROW IN A TABLE.  
* Most DB platforms have a PK created with an index on it.  This index DOESN'T define uniqueness.

* Creating an index means creating a physical object which is being saved to disk. 

*PK logical, Index physical*  
 
## Using EXPLAIN & Query execution plan
* `EXPLAIN` provides information about how DB optimiser executes statements.
* Returns a table of values with output columnd. We want to minimiddr NULLS.

* ID, a squenctial identifier for each SELECT within the query, for when you have nested subqueries.  
* Tablr, table referreed to by the row
* Possible_keys, shows the keys that can be used the optimiser to find rows from the table. If NULL, no relevant indexes were found.  
* LOOKUP others.  

## Demo  
### Poor DB + Poor Query
DB has no indexes or PK.  
*This means need to `INNER JOIN` each table together on correct column manually to get the needed  explanation*
`EXPLAIN` shows:
	*  simple query, no nested queries.
	* Lot of rows
	* NULL for possible keys
* We had to scan ~800million rows to return 4, 

### Improved DB with PK
* Look at columns used in JOIN clauses of the query as a candidate for keys.  
`EXPLAIN` shows:
	* possible primary keys
	* fewer rows to search
	* `JOIN TYPE` is const, can return values straight away. 
* We had to search 4 records to find the matching results.

### Using UNION
UNION of two tables. product and product variants, each joined with product line.  
Product variant table consists of different product variants with productCode as reference keys and their prices.  
	* Select type is derived as it is a subquery.  
### Using indices
Check query, but these can significantly increase your speed.  

## Remember
* In the real world, more often than not you'll be joining a number of tables using complex WHERE clauses.  
* Adding indexes on a few columns might not always help
* Most of the time it is helpful to look more closely at your queries.  


# Lec 8 The DB Language-Transactions
## Transaction Concept
* A transaction is a unit of program that accesses and possibly upates data items.  
* "A collection of SQL queries which form a logical task."  
* Transaction is either completely executed or not at all, if one query fails then no queries are run.  
**This is required to protect data and keep it consistent when multuple users access the data base at the same time.  

Main issues to deal with:
* Failiures due to hardware or ststem crashes.
* Concurrent execution od multuple transactions.

### Requirement
Atomicity, The system shoul ensure that the updates of a partially executed transaction are not reflected in the data base.  
Consistency, ???
	* This requires explicitly specified integrity constraints such as PK and FK	
	* Implicit constraints, sum of all balances in all accounts minus all loas must = cash in hand.  

Isolation, must make transactions isolate to ensure consistency. Can be assured trivially by running transactions serially (We would like things to be run concurrently though.) Although multiple transactions may be run concurrently they must be unaware of eachother to have a consistent DB.

Durability, updates to the atabase by the transactions must persist.  

**Generally**
* Transaction myst see consisten DB on execution
* During execution, temporary inconsistency allowed
* Upon finishing a transaction, consistency must be restored.

### Transaction state

## Transaction implementation
