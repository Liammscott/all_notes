# SDLC- Software development lifecycle
Planning, creating, testing, and deploying software.  

## Measuring complexity
* Lines of codes
* Number of classes
* Number of modules
* Module dependency
* Comprehension time
* Legacy documentation

## Why SDLC
* Essential complecity
Technical complexity: due to us and our bad design!
Accidental complexity! (Improve by using suitable languages)
Requirements complexity: Perhaps we're offering the user an unnecessary requirement/feature.  

## What SDLC?
SDLC helps decide order of: requirements, design, implementation, testing, maintenance
As well as their combination

## Software Dev vs Software Eng
Dev: Individual activity, ad hoc methods, build now plan later, high risk
Eng: Team activity, established principle, plan before build, predictable  

## How to conduct SDLC/SDLC models
each model has pros and cons. Hybrid models are usually best. Aimm for: good, fast, cheap development.  

## Waterfall model: A sequential approach
Req analysis>Design>Implementation>Testing>Maintenance
*No iteration, purely sequential*
**Con: Uncertainty, impossible to refactor, not suitable for large projects**
**Pro: Simplicity, Better from cost management, Suitable for small project**

## Iterative methods: Unified Process
Inception (Scope, reqs, business case)> Elaboration (Func req, architecture, risk analysis, portfolio management)> Construction > Transition
**Con: Costly to manage, change in req can cause costly architecture, not good for small projects**
**Pros:Measurable progress, a little bit of flexibility on reqs, easy to do risk analysis**


## Spiral model: Waterfall + Iterative + Risk analysis
1. Objectives determination and identify alternate solutions
2. identify and resolve risks
3. Develop next version of the product
4. Review and plan next phase
REPEAT. Each new phase adds to cost

More appropriate for domains in which risk analysis is crucial
Considers big risks early
Risk decreases and cost increases  

**Cons: Very complex due to excessive sub steps, costly documentation, overkill for small projects, unknown number of phases**
**Pros: Comrehensive documentation, good risk analysis, customer feedback**

## V-shape: waterfall + validation/verification  
Req analysis, Specs, high level design (uml notation, a model/architecture), Low level design, Implementation
Testing, acceptance testing, Integration testing, Unit Testing
(One to one testing to level)
**Cons: testing is costly, not flexible, requires dedicated testers, needs strict pre defined reqs**
**Pros: Validated results at each stafe, early testing and validation, good for small projections**

## Agile model: Dynamicity over ceremony
*The change from process centric to PRODUCT centric*
We are about flexibility in the face of changing customer reqs.

Req>Design>Implementation>Testing and feedback

**Cons: Architecture conflict with new reqs, wrong prioritisation (Break reqs down into sprints, need to prioritise which reqs need doing first), Uncertainty in project time**

## Modern Paradigm of SDLC
* Working prototype is better than documentaition
involvement of customer in ever step of development
learn to program not to code(consider legacy, rest of software not just your part)
expect change in architecture due to changein reqs
reuse modules (efficiency)
50-60% of software eng is trying to maintain code

M- Must have
O
S- Should have
C- Could have
O
W- Would have

## Extras
XP (Extreme programming)
Basics of scrum
DevOps
Learn
scrum

## Summary
An appropriate model is dependent on nature and domain of complexity
desirable paramerters are fast cheap and reliable
budget vs risk analysss and validation
not more tan 20% ceremony


# Project failiure Spectrum
More expensive to operate, late/over budget AREN'T failiures  
solution largely unsused, significant rework required, project cancelled ARE Failiures 

# Lec 2
4 main phase  

## Requirement engineering
### Getting started
* Based on initial documentation, find something you can build with confidence. * INVOLVE THE CLIENT.
> Often the client doesn't know what they need, our first job should be to try and identify this.  

### What is req eng?
* solicitation, where the company gets specs from client
* analysis
* specification
* verification
* process

### Types of Reqs
* Functional, what is the system *meant* to do? **This is the most important**.
* Usability
* Reliability 
* Performance 
* Supportability 

## Prioritisation
**MoSCoW**  
> Because building the right software isn't the same as building a software right.  

### Estimating effort
* New paradigms offer alternatives to estimate software dev eff, Computational Intelligence. 

## Domain Engineering
> This is for the customer, it isn't UML or architectural based. It is a sanity check.Low cost, high value ceremony.

### Minimalist approach
* Names consistent with stakeholders terminology.
* Emphasise entity relation.  

### Naming convention
Use nouns for entities, verbs for relations.  

> An ERD diagram is usually good for a domain engineering model.   


## Design engineering
> More costly than domain. High cost high value ceremony.
* Goal, high cohesion low coupling.

### Examples of fabrication
* Interfacecs to external databases.
* UI components.
* Abstraction classes.

### Good code vs. Bad code, GRASP
* General Responsibility Assignment Software Patterns
* Principles: 
	* Creator
	* Info expert
	* Low coupling
	* Controller
	* High cohesion
	* Indirection
	* Polymorphism
	* Protected variations
	* Pure fabrication  
* **Creator:** defines who is responsible for creating the object.  
> e.g. Class B only creates instance of A if:
	* B contains A
	* B shares most features of A
	* A extends Bw
* **Info Expert** It emphasises the need to assign responsibilities to the right expert, e.g. ChessBoard vs. ChessPiece. Define behaviours in proper places.  

* **Low coupling** Coupling is measuring how closely objects are tied to each other.  
* **Controller** A controller defines how to delegate request from UI layer objects to domain layer objects
	* MVC
		* Model, structures in reliable form and prepares it based on controller instructions
		* View, Displays data in easy to read way. 
		* Controller, takes commands, sends commands to the model ffor data updates, sends instructions to view update interface.
Best controller has as little business logic as possible. Should perform methods, such as queries, in model.
* **Pure fabrication** interface/abstract classes used to acheive high cohesion, low coupling.  


## Testing
* Testing is a search problem
* Embury's law: "software has bugs, we just can't see it yet."  

### Black box
Test without seeing the codebase, just checking interface without accessing code.  (Like Simon's with going off screen and winning game)

### Unit testing
Test a specific feature/function in each class. Classes use other function, so need to check these functions first.  

### System testing
Testing the system in context where it should work. 

### Alpha testing
testing from devs pre beta

### Beta testing
Application given to real users and ask them to submit bug reports

### Acceptance testing
Get customer to find bugs

### Smoke testing
To check after addition of a feature to check if there is any issue with the system due to ripple effect.  





> How is cohesion different to info expert
> Try implement MVC yourself
> Learn principles of GRASP
> Is system testing part of AGILE
