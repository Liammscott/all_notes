#  Python
This will contain a list of functions and techniques i've found useful
##Concepts
* **Abstraction:** Large systems can be understood in levels, abstraction allows 	us to talk to lower level hardware in terms we can understand.
* **REPL:** Re-evaluated print loop, allows for line by line printing and testing
* **Objects:** Things with a type *type(object) can reveal type*
* **Scalar:** a type that can have a single value int, float, bool, none
* **Floats:** *some more notes on this would be good*
* **Boolean operators:** And, or, not
* **Alias:** Assign value to string *e.g. pi=3.14195*



##Chapter 5
===========
* **Tuples:** 
  *  immutable ordered sequence of elements that do not need to be of the same type.
    * outputted as, for example, ('a','1','b', '2', 'c', ...)

* *Range:* 
  * immutable like *str* and *tuple*
  * defined by *range(start, stop, step)*, 3 *int* parameters.
  * Range equality if two ranges show same numbers.
* **List:** 
  * similar to *tuple* but use [] ('[]').
  * *list* is also mutable, once.
    >Object is created, it is assigned a unique object ID. Type is defined at run time and can not be changed after creation.
    * **list** commands:
      * *L.append(e)* adds e to the end of L
      * *L.count(e)* returns number of times e occurs in L.
      * *L.insert(i,e)* inserts the object e into L at index i
      * *L.extend(L1)* adds the items in L1 into end of L
      * *L.remove(e)* deletes the first occurrence of e from L
      * *L.index(e)* returns the index of the first occurrence of e in L, exception raised if e not in L.
      * *L.pop(i)* removes item at index i in L.
      * *L.sort()* sorts elements of L in ascending order
      * *L.reverse()* reverse order of elements in L.

> You should avoid mutating a list you are iterating over. *e.g. for L1=[1,2,3,4,5,6], L2=[1,2,3,4,5,6]* if we try to remove duplicates as we iterate, the first element will be removed, making L1 a shorter list than L2 disrupting our counting.

* *Cloning:* make a copy of the list and check for e1 in L1[:]. 
* *List Comprehension:* Concise way to apply, an operation to the values in a sequence. Creates a new list in which each element is the result of applying a given operation to an entry. *e.g. L=[x'**' for x in range(1,7)]*
> You should comment when you use list comprehension BC it is subtle and hard to see what is happening.

* *Functions as objects:* 
  * functions are **first-class objects**, they can be treated as objects of any other types.
  * Using functions as arguments allows for higher order programming.

* *map:* 
  * takes a function and applies it to the argument.
  * *map(f,i)*

* *Lambda:*
  * an anonymous function, attached to reserved word lambda
  * *lambda <sequence of variable names>: <expression>*

> Having learned about *str*, *tuple*, *range*, *list*, we will look at how they differ/are similar and their functions.

* **Shared functions:**
  * *seq[i]* returns ith element in sequence.
  *  *len(seq)* returns length of sequence
  *  *seq1+seq2* returns concat *NOT for ranges*
  *  *n seq* returns a sequence that repeats seq n times *NOT for ranges*
  *  *seq[start:end]* returns slice of sequence.
  *  *e in seq* True if e is in seq.
  *  *e not in seq*
  *  *for e in seq* iterates over sequence elements.

* **String functions:**
  * *s.count(s1)* counts how many times s1 occurs in s
  * *s.find(s1)* returns the index of the first occurence of substring s1 in s (returns -1 if s1 not in s).
  *  *s.rfind(s1)* same as find, but starts from the end of s.
  *  *s.index(s1)* same as find, but raises exception if s1 not in s.
  *  *s.lower()* converts all uppercase to lower case.
  *  *s.replace(old,new)* replaces all occurences of the string old in s with string new.
  *  *s.rstrip()* removes trailing white space from s.
  *  *s.split* split using a delimited. Returns *list* of substrings of s.

* **dict** 
  * dictionary, are like lists but indexed with *keys*, dictionaries are key/value pairs.
  * *dict={key:value}*
  * The order in which keys are iterated over is not defined, they are **view object**.
  > A literal is something recognised by the parser as syntax for writing an object directly.

## Chapter 6
============
>
>  * *Testing:* Running a program to see if it works as intended
>  * *Debugging:* Fixing a program you know does not work as intended

*Good practice is to test and debug as we go!*
* ***Test suite:*** Collection of inputs that have a high probability of crashing the program and do not take too much time to run.
* ***Partition:*** Each element of the original set belongs to exactly one of the subsets
> Good partitioning of inputs is easier said than done: heuristic based on exploring paths through are used *(This is called glass box testing)*, as well as heuristics based on exploring paths through the specification *(Black box testing)*

* ***Black box testing:*** 
*This can be done with out looking at the code, implementers and coders can be from separate populations.

* ***Glass box testing***
* *Blackbox* testing is rarely sufficient, without looking at the code it is difficult to decide on new cases to test.
* >Path complete: A glassbox test is said to be path complete if it explores every possible path through the code. *Typically path-complte test are impossible (consider the depth of each recursion and iteration of each loop)* **Path complete tests are not guaranteed to return all bugs**
*  ***Conducting tests***
    * **Unit testing** tests designed to test individual sections of code (functions)
    * **Integration testing** tests designed to see if the program as a whole works as a whole. *This can be very difficult, due to problems of scale*
**Debugging**
* **Overt and covert:** *overt* has obvious manifestation, *covert* has no obvious manifestation *e.g. program runs its full length with no indicator of failure other than an incorrect answer*
* **Persisten and intermittent:** A *persistent* bug occurs every time a program is run with the same inputs. An *intermittent* bug occurs only some of the time.
> *covert* bugs are the most dangerous, as people come to rely on the program because it works.

# Chapter 7: Exceptions and Assertions
**exceptions:** usually error related, cases raised by python when it has an issue running a code.

## Handling exceptions


> an **unhandled** exception causes the program to crash. Exceptions *should* be **handled** when they are raised. We use "try:, except:" blocks of code to handle exceptions.
```
try:
  some_block_of_code
except error:
  do_this_block_of_code
```
If an ```except``` block is used with no specification, then the block will be entered if any sort of exception is raised.

## Exceptions as a control flow mechanism
It is normal to have a function return an exception when it cannot produce a result consistent with the functions specification.
```
   raise exceptionName(arguments) 
``` 
```
   exceptionName ``` is usually a buit in exception, but nre exceptions can be defined using a subclass
   ```

## Assertions
The assert statement is a simple way to confirm that the state of a computation is as expected
```
   assert Boolean_expression
   ```
or

```
   assert boolean_expression, argument
   ```
with an Assertion Error raised if the Boolean evaluates to a false.  

# Chapter 8, Classes and Object Oriented programming
## Abstract Data types and classes
**ADT:** An ADT is a set of objects and operations on those objects. These are bound together and passed around the program and are easy to access.
>*Operations provide an **interface** between the ADT and the program.*  
*The **interface** provides an **abstraction-barrier** isolating the rest of the program from the ADT.*  

**classes:** The method by which we implement data abstraction. Below an implementation of a class is shown.   
```
class IntSet(object):
  def __init__(self):
    set.vals = []
  def some_other_functions (self, parameter):
    pass
   ```
* **Instantiation:** **class** operation to create new instances of a class. e.g. `i = int(1)`, `i` is an **instance** of `int`.  
*  **Attribute references:** Use dot notation to access attributes associated with the class.  

The `__init__` function: When a class is instantiated a call is made to the `__init__` function defined in that class. 


> The represenatation invariant: defines which values of the data attributes are valid representations of class instances. Consider `intSet`, vals = [] should contain no duplicates. __init__ sets up representation invariance, other methods maintain invariance. Other functions then exploit the representation invariance.  


*Consider a program as a collection of types rather than a collection of functions.*  

## Inheritance  
**Inheritance** alows us to construct a class heirarchy, with each type inherting function from the type it is derived from. The **subclass** adds newclass and instance variables, as well as methods.  **Subclasses** also overwrite existing functions.  
At runtime a program will check for functions of a given name firstly associated with the class, then with its super class.  

## Multiple levels of inheritance
Can add classes of format:  
```
   class Student(MITPerson):  
      pass
   ```
to help make hierarchy.  
*Good programmers design code to minimise amount that might need to be altered later on.*  

**Substitution principle:** when subclasses are used to extend a hierarchy, the subclasses should be thought of as extending the behaviour of their superclasses.  e.g. *code that works on Student should work on TransferStudent(Student)*  

## Encapsulation and information hiding  
**Encapsulation:** we bundle together data and data operations.  
**Information hiding:** If clients of a class only rely on the specifications of methods in the class, a orogrammer implementing the class is free to change the implementation of the class without worrying about the code breaking.  

**Private in python**  
*attributes beginning \_\_ (NOT ending \_\_) are invisible outside of the class*  

# Chapter 10, Some simple algorithms and data structures
**Search algorithms:** method for finding a group of items with specific properties in a collection *The collection is called the search space.*  
**Indirection:** The act of accessing something by accessing something else first. e.g. *access memory where list is, then iterate to ith entry*  

## Binary search and exploiting assumptions
We can improve worst case scenario list searching by using **binary search** algorithm, similar to biseciton search algorithm. 

**Wrapper:** 
```
def search(L,e):
  ""assumes L is a list, the elements of which are in ascending order. Returns True if e is in L and false otherwise."""
  def bSearch(L,e, low,high):
    if high == low:
      return L[low] == e
    mid = (low + high) * 0.5
    if L[mid] == e:
      return True
    elif L[mid] == e:
      return True
    elif L[mid] > e:
      if low == mid:
        return False
      else:
        return bSearch(l,e,low,mid-1)
    else:
      return bSearch(L,e, mid+1, high)
  if len(L) == 0:
    return False
  else: 
    return bSearch(L,e,0,len(L-1))
   ```
In the above code the function search provides a nice interface for the client code, but actuall calls bSearch to do the brunt of the work.

*Bisection searches are actually O(log(L)). This is because we divide x by y log_y(x) times we get 1 (the searched for element)*  


> General tip: It is often a good strategy to start by solving the problem in hand in the most straightforward manner possible, instrument it to find any bottle necks, then look for ways to improve the computational complexity of those parts of the program contributing to the bottlenecks.  

## Sorting algorithms  
**Merge sort**  
Using divide and conquer can sort lists in *nLog(n)*  
*TimSort is used, taking advantage of the fact most data is already sorted.*  

## Hash Tables
Hash tables are a form of **many to one mappings**, keys are assigned to each value and a hash is found from this key, which determines which **hash bucket** the value key pair is stored in.  
**Collision handling** tries to ensure the list of key value pairs in each hash are minimal, keeping look up times ~constant.  

# Chapter 11 Plotting and more about classes

