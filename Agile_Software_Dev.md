# Things that go wrong
"It doesn't do what we asked!", Mis-understood reqs
"It's late"
"It's got bugs!", features there but don't work

# Why they go wrong
* Late feedback, telling us after deadline.
* Developing in too large pieces, whole projects (Waterfall dev).  

# What can we do
* Seek earlier feedback
* Show ongoing work to stakeholders
* Use thefeedback to drive development
 
## Smaller pieces
Big enough to show stake holders.  
As small as can be and still be critiqued usefully.  
We bank value during production, additive iteration.  

*The product we deliver can be quite big, UI, business logic, persistent database*  

> "A requirement describs a computer program, it decribes your work. A story describes a user going through a domain level process or activity it describes your user's work. These are very different things." - Allen Holub

Advice: 
	* Practice to learn
	* Slice and slice again
	* Remember why your doing this, get feedback from the stake holders.  
	* Explain to stakeholders so they understand why.  
*agileforall.com/patterns-for-splitting-user-stories*

## Story prioritisation
* Work on most valuable first  
	* partially complete can be used before finish  
* Investigate big risks first  
	* Deliberate Discovery (Dan North)
	* If we can learn a proposal is not viable early, that is a big win!

* Stake holders often want it all
	* You need to help them understand why we use the most valuable stories first
	* They should see that being 'in the loop' and having their feedback acted upon is better.  

## How does this affect our process
* expect to be learning at all times
* we should expect the stake holders to learn too
* we should expect reqs to change
	* we could add or remove reqs.
	* we could change direction completely.
	* being able to respond to this quickly is agility. 

## How does this affect our coding
* We will be regularly adding to the codebase
* We need to keep completed work working
"growing a garden" rather than "building or construction"

*We need to optimise our working/coding process to be optimised for this sort of development*  

## Cost of change
In a sequential waterfall approach, cost of change exponentially rises.  
XP changes this, give ideal log like growth that flatlines rapidly with time.  
*Requires keeping code non-complex*

## The technical premise of XP
"If cost of change rose slowly over time, you would act completely differently ro how you would under the assumtpion that cost rises exponentially.
You would make big decisions as late in the process as possible, to defer the cost of making the decision and yo have the greatest possiblity of them being right." - Kent Beck, XP explained.  

## Minimising code complexity
"Cost of change rises more steeply without objects than with"- Read Sandi
(Objects are good)
"A simple design with no extra design elements", YAGNI(YouAin'tGonnaNeedIt)/TDD  
"Automated tests, so we had confidence we would know if we accidentally changed the existing behaviour of the system"  
"Lots of practice in modifying the design, so when the time came to change the ssystem, we weren't afraid to do it"

> katas, exercism.io
> TDD by kent beck.  

## 4 variables
Cost: 
	* more money = easier, faster machine, more staff
	* But too many cooks spoil the broth
Time:
	* more time can lead to a higher spec project
Quality:
	* Important variable, not really free, an indicator.  
Scope:
	*

## Simplicity
"When I am working on a problem, I never think aout beauty but when i have finished if the solution is not beautiful, I know it is wrong." - J Buckminster Fuller

## Manifesto for agile software dev
PASTE

## Agile at THG
* What is our process?
* You should ask many people?
* You should think about what you see
	* Do the projects suceed.
	* Do we learn from failiure.
	* Do you see stakeholders giving feedback.
	* Do we change direction in development.
	* Are we ale to react quickly to change.
	* Do we feel the pain of running our systems.

# Whimsical point
Agile software dev is just software dev done right
