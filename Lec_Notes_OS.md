# UTF-8 vs ASCII
## ASCII:
## 


# Last time
Discussed: how python program is compiled to bytecode and executed in virtual machine inside the python interpreter.  
Talked about Linux processes in general.  
Each process runs its own protected virtual address space.  
Linux scheduler swaps processes in and out of run state.
System calls and blocking.  
Every process except the first formed by cloning an existing parent process and usually loading a new program into the new child process (fork/exec).
*All processes come from an initial process except the `init` process*  
We started to talk about how the kernel does "Multiprocessing" *The scheduler problem*.

# Demo shell program

Unix Kernel
Stack segment
Free Space
Data segment
text segment

> Cambridge OS course.
## What does bash do?
* Sits around, waits for you to type, usually asking to run a program.  
* Breaks down your command into tokens based on whitespace.  
*Assume first word is program, the rest are arguments*  
> Note: If you write you own bash, you'll realise alot of the functionality (`ls`, `emacs` are separate programmes).
> Note: can't change directory, as directory is property of current process. You're trying to change your own directory, need to make a child and change it's directory.  

## Processes from the kernels perspective
* Kernel has a table of processes, each process points to a C structure (A class without methods). This is called `task_struct` contains all the info Linux needs to run your process.  
* Set of `task_struct` structures are linked in hash table called `pid hash`, keyed by process id. (Also linked in linked list.)  
* Old days this was alist that could fill up.  
> Note: DOS attack 'fork bomb', can recursively fork and can fill up `task_struct`  

## Struct `task_struct`  
* Includes processor state that must be saved when process isn't executing:
	* Program counter
	* Stack Pointer
	* General Purpose register
* It also includes:
	* Open files
	* Pending signals
	* Process counting
	* Process owner, permissions, working directory, args, env,etc.
	* Threads of execution.  

## Threads  
There is a stack pointer: Where is the environment for the currently running function?
* Instruction pointer: What do I do next?
* Function call: jump instruction pointer to the starting addresses of the function, adjust the stack pointer to the end and give it space to run the function.
* One thread works on one thing, another on another, OS swaps between.  
* Threads are efficient as they share context, each thread can access each others memory.  
* *This context sharing can be bad: two threads operating on same thing, not sure when one will stop and another will start you can get *race conditions*.*


> We will look more at threads in java, as this is a large part of the thing.  


# Process tools  
* `/proc`: `ls -l` gives a load of process IDs.  
	* These process IDs are the thing in the task struct.  
* `proc/'ProcessID'/fd`: file descriptors, what files are run by this process?  
> The `ps` command just drives around this `proc` file structure.  
> `ps -ef`: tell me EVERYTHING running on this machine.  
> **Super special** ProcessID=1 has no parent. This is the `init` process for starting and shutting the system.  

## Context switching  
To switch between processes, teh OS must:
	* Save the context of the currently executing process if any and restore the context of that being resumed.  
	* This is WASTED time.  
	* Modern CPUs have stuff to make this efficient. (Lol this is very vague)  

## Scheduling  
* The OS kernel has access to hardware timer that gives it control periodically
*Modern preemptive OS fires periodically giving control to the kernel. Should I run the value running or another*  
* Scheduler maintains a value for each process with its priority, saying which should I run next? When it wakes up it will compare the merit of the one it was running with the next one.  
* When a process makes a blocking system call the kernel sets the process to "waiting" and context switches.  
> Note: look at notes.shichao.io/lkd/ch4/  
> Nice value: Customise your processing power, if you have something computationally expensive you can specify you want it to run at a lower priority.   



*Cool diagram here from the notes URL above*  

## Signals
* Ctrl+c is one of these. Tell program running terminal to interupt running process.
* Signals have number, there are 32 of them with default semantics. Each one of these is just a bit.  
* Signals are how processes interact with each other.
* Signals are sent to a process, the kernel sets the signal bit in the processes bitfield.
* Bitfield: Array after process, if all zeroes then no signal. If signal is sent then that element is updated.  
* Consequences: If the process is set multiple signals that are the same you can't stack them. You can also have several signals at the same time.  

## Signal handling  
* Signals have default behaviours, some do nothing.  
* We can override the signal default behaviour with a custom handler function.  

## SIGKILL(9)
`handler(signum, frame)`: takes a signal number. Installing handler for `SIGTERM` (kill) and `SIGALARM` (timer).

`signal.alarm(5)`: send alarm in 5.  
`text= input(blocking stin from input)`  

> Signals are not content rich, just a single number 'poke' saying "i'm sending you a number you can do something with".  

## Common signals
* *SIGHUP*: sent when controlling terminal terminates (originally phone line hangup). Important because program could be busy doing something complex and we want to save some state.  
* *SIGINT*: ctrl-c from terminal.  
* *SIGILL, SIGSEGV, SIGFPE*: SIGFPE, SIGFloatingPointExecption to trap div by zero and handle.
* *SIGALARM*: Sleep calls alarm, OS says "don't do anything with me until the timer is called".
* *SIGWINCH*: Resize window, listen for a resize then re draw using the whole space.   

> Daemons detach from terminal their run on and from parent process and move to the init as the parent. Good for servers/things you want to run in the background. Alot of things that make Linux run are Daemons.
> Basically: SIG is sent to bitfield, asking Kernel "what do you want to do about this?" it asks the process that made the issue and executes the handler. If there isn't one then the OS default is set up.  

# Lec 2: Introduction to programming/Memory management

## Concepts
### DRAM Physical Memory
	* Memory has a load of pins, ~250, connected to bus. CPU talks to Bus and can talk to memory by address.  
	* Bit is stored as charge in a tiny capacitor. Capacitor leaks slowly, so we have to refresh, hence Dynamic.  
	* Random access, not sequential, can access any address equally quickly.  
	* Volatile data disappears when power is off.  

Every process needs memory for:
	* instructions, text in diagram
	* place to store data that is known at compile time. gloabal/constants/static
	* Dynamic data, heap/stack, dynamically allocated variables and also function calls.  
**OS also needs memory, because it's a program!**

### Relocation
DRAM has memory addresses 0 to MAX. To read data you need to know where it is in this list of addresses.  
BUT programs don't know where the data is stored at runtime.  
* The OS is managing alot of stacks, and may want to swap processes in and out of memory to optimise CPU usage.  
* Inefficient to require a swapped in process to always go to the same place in memory.  

### Protection
* Need to protect each process from others, make sure memory isn't shared and overwritten.  
* Might want to give different chunks of memory differnt permissions, so the memory needs to have instructions written to it. read write execute.  

### sharing
* If we run many instances of the same program, we should only keep ONE copy of text they all use. Shared binaries
* Can precompile different code in namespace, can load them into text at runtime so we can access. Shared Libraries
> In usr/lib, on mac they are called dylib (dynamic lib). Libraries that handle print, maths, and are precompiled and bound to program at runtime.    

* We can share memory between processes

### Logical Organisation  
As program runs, it's need for memory will increase. How do we allocate more memory?  
### Physical Organisation
OS can manage overflow of memory into second storage, allowing programs to run more memory than is physically available.  
	* Swapping
	* Paging  

### Address binding problem  
Consider: a code fragment
```
int a = 2;
int b = a+3;
```
generates instructions like
```
store 2 in address a
load contents of address a into register R1
add 3 to R1
store R1 to address b
```
but how do we know what address to use for a and b? we don't know this at compile time.  

### Address binding solution  
in DOS only 1 program at a time.  
* Compile time, require absolute address knowledge, like in DOS.  
* Load time. update addresses in program after loading, when physical address is known.
* Run time, program operates in own address space in the stack. At run time these addresses are mapped into physical memory by CPU.  

### Virtual to physical mapping
map these process addresses to physicalin run time.  

### Memory management functionality
* Physical memory organised in 8kB pages in 64 bit.  
* MMU maps process view, virtual address, page to physical page by referencing a maping table.  
* 8kb is 8192 bytes, 2^13 so with 13 bits you can reference where you are in a page.
* The left over 49bits can be used to reference pages. 10^14 pages!

Machine loads, splits ram into 8KB pages, gives status about if their in use or not.  
Kernel has mapping table, processor has a copy in cache as well called translation lookaside buffer.

### Page meta data in `struct page`
Set up when system boots for each page of physical DRAM.
Contains:
	* count of page references
	* physical addresses
	* virtual addresses  

### VRAM features
* Paging and page faults:
	* Linux can store contents of pages that haven't been used recently on secondary storage and reuse the RAM.  

* Shared memory, CopyOnWrite:
	* `fork` clones existing process, Kernel makes a new one with same data. This used to be inefficient. With VRAM we don't copy data, we just set up page table so data of child process points to the memory of parent process. This is dangerous because parent and child can write to same space.  
	* Copt on write, data segments can be shared, but marked as copy on write such that a separate copy is made as soon as parent or child writes to it.  
COW useful for working on same data with multiple processes, allowing for multi threading.

* Memory mapped files:
	* Paging mechanism can be used to map files on disk with pages of memory. So we don't need to keep whole file in memory, just move it in when it is called.  
	* Multiple processes can map the same file, sharing memory.  
	* Special /dev/zero used to allocate zeroed memory.  Allows us to allocate memory that is clean and only 0s.  

###  Latency numbers
* L1, fastest read cache on the chip, 0.5ns, bout the speed instructions are given
* Disk seek, reading the HDD using physical mechanism, 10ms
> Note: L1 is 10^5 times faster than disk.


COMPUTER DIAGRAM:
kernel
^
stack
^
heap
^
Data
^
text


##Motivations



>Learn about stack.
>Do all OS only have 32 Signals? why aren't there more?
>User input on a separate thread rather than timer?
